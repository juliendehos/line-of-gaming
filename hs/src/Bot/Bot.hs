module Bot.Bot where

import System.Random

import Core.Params
import Game.Game

class Bot b where
    getRefParams :: b -> Params 
    newBot :: Params -> StdGen -> b
    chooseAction :: Game g => g -> b -> (Int, b)

