module Bot.BotRandom where

import System.Random

import Bot.Bot
import Core.Params
import Game.Game

newtype BotRandom = BotRandom { _gen :: StdGen }

instance Bot BotRandom where
    getRefParams _ = emptyParams

    newBot p = 
        if p == emptyParams
        then BotRandom
        else error "BotRandom has no params"

    chooseAction g b =
        BotRandom <$> randomR (0, getNbLegalActions g - 1) (_gen b)

