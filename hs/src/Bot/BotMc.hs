module Bot.BotMc where

import Data.List (foldl')

import Bot.Bot
import Bot.BotRandom
import Core.Params
import Game.Game

data BotMc = BotMc
    { _kWin  :: Double
    , _kTie  :: Double
    , _kLose :: Double
    , _nSims :: Int
    , _bot   :: BotRandom
    }

botMcParams :: Params
botMcParams = fromList
    [ ("kWin", 1)
    , ("kTie", 0.75)
    , ("kLose", 0)
    , ("nSims", 10000)
    ]

instance Bot BotMc where
    getRefParams _ = botMcParams

    newBot params gen =
        let params1 = mergeParams botMcParams params
            kWin = getParam params1 "kWin"
            kTie = getParam params1 "kTie"
            kLose = getParam params1 "kLose"
            nSims = floor $ getParam params1 "nSims"
        in BotMc kWin kTie kLose nSims (BotRandom gen)

    chooseAction game botMc = 
        let nActions = getNbLegalActions game
            nRollouts = max 1 (_nSims botMc `div` nActions)
            (r0, bmc0) = evalAction game nRollouts 0 botMc
            f (iBest, rBest, bmc) i =
                let (r, bmc') = evalAction game nRollouts i bmc
                in if r < rBest then (iBest, rBest, bmc') else (i, r, bmc')
            (iBest1, _, bmc1) = foldl' f (0, r0, bmc0) [1.. nActions-1]
        in (iBest1, bmc1)

evalAction :: Game g => g -> Int -> Int -> BotMc -> (Double, BotMc)
evalAction g0 nRollouts k botMc =
    let s0 = getStatus g0
        g1 = applyAction k g0
        b0 = _bot botMc
        f (r,b) _i = let (s,b') = rollout g1 b in (r + evalStatus botMc s0 s, b')
        (r1, b1) = foldl' f (0, b0) (replicate nRollouts (0::Int))
    in (r1, botMc { _bot = b1 })

rollout :: Game g => g -> BotRandom -> (Status, BotRandom)
rollout g0 b0
    | isPlaying g0 =
        let (k, b1) = chooseAction g0 b0
            g1 = applyAction k g0
        in rollout g1 b1
    | otherwise = (getStatus g0, b0) 

evalStatus :: BotMc -> Status -> Status -> Double
evalStatus botMc Play0 Win0 = _kWin botMc
evalStatus botMc Play0 Win1 = _kLose botMc
evalStatus botMc Play1 Win0 = _kLose botMc
evalStatus botMc Play1 Win1 = _kWin botMc
evalStatus botMc _     Tie  = _kTie botMc
evalStatus _     _     _    = error "BotMc: evalStatus: game not finished"

