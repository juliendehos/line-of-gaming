module Core.Params where

import Data.List
import Data.List.Split
import Text.Read
import qualified Data.Map as Map

type Params = Map.Map String Double

emptyParams :: Params
emptyParams = Map.empty

getParam :: Params -> String -> Double
getParam = (Map.!)

fromList :: [(String, Double)] -> Params
fromList = Map.fromList

parseParams :: String -> Params
parseParams "" = emptyParams
parseParams str0 =
    let parseParam str = case splitOn "=" str of
            [keyStr, valStr] -> case readMaybe valStr of
                Just val -> (keyStr, val)
                Nothing -> error "parseParam: not a number"
            _ -> error "parseParam: not a 'key=value'"
    in fromList $ map parseParam $ splitOn "," str0

mergeParams :: Params -> Params -> Params
mergeParams refParams tryParams =
    let updateVal k v params =
            if Map.member k params
            then Map.insert k v params
            else error $ "parseParams: unknown key: " ++ k
    in Map.foldrWithKey updateVal refParams tryParams

showParams :: Params -> String
showParams params =
    let fmtKeyVal (k,v) = k ++ "=" ++ show v
    in intercalate "," $ map fmtKeyVal $ Map.toList params

