module Game.GameFind3 where

import Game.Game

data GameFind3 = GameFind3
    { _history :: [Int]
    , _status  :: Status
    } deriving (Eq, Show)

instance Game GameFind3 where

    newGame = GameFind3 [] Play0

    applyAction i g0 = 
        if i >= 0 && i <= 9 && isPlaying g0
        then
            let (GameFind3 h0 s0) = g0
                h1 = i : h0
                s1 | i == 3 && s0 == Play0 = Win0
                   | i == 3 && s0 == Play1 = Win1
                   | length h1 == 4 = Tie
                   | otherwise = if s0 == Play0 then Play1 else Play0
            in GameFind3 h1 s1
        else g0

    getNbLegalActions _ = 10

    getStatus = _status

