module Game.Game where

-- TODO implement player0/player1 using a type ?

data Status = Play0 | Play1 | Win0 | Win1 | Tie deriving (Eq, Show)

class Game g where
    newGame :: g
    applyAction :: Int -> g -> g
    getNbLegalActions :: g -> Int
    getStatus :: g -> Status

    isPlaying :: g -> Bool
    isPlaying game = case getStatus game of
        Play0 -> True
        Play1 -> True
        _ -> False

