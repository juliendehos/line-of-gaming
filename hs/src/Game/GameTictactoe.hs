{-# LANGUAGE BangPatterns #-}

module Game.GameTictactoe where

import qualified Data.Vector.Unboxed as U

import Game.Game

data Action = Action Int Int deriving (Eq, Show)

data GameTictactoe = GameTictactoe
    { _board        :: !(U.Vector Char)
    , _legalActions :: ![Action]
    , _status       :: !Status
    } deriving (Eq)

instance Game GameTictactoe where

    newGame = GameTictactoe 
                (U.replicate 9 '.')
                [Action i j | i<-[0..2], j<-[0..2]]
                Play0

    applyAction k g0 = 
        if k >= 0 && k < getNbLegalActions g0 && isPlaying g0
        then
            let (GameTictactoe b0 a0 s0) = g0
                (xs, y:ys) = splitAt k a0
                a1 = xs ++ ys
                (Action i j) = y
                c0 = if s0 == Play0 then 'O' else 'X'
                b1 = b0 U.// [(ind i j, c0)]
                s1 | testWin b1 i j = if s0 == Play0 then Win0 else Win1
                   | null a1 = Tie
                   | otherwise = if s0 == Play0 then Play1 else Play0
            in GameTictactoe b1 a1 s1
        else g0

    getNbLegalActions g = length (_legalActions g)

    getStatus = _status

ind :: Int -> Int -> Int
ind i j = i*3 + j

testWin :: U.Vector Char -> Int -> Int -> Bool
testWin b i j
    =  f i 1 == f i 0 && f i 2 == f i 0
    || f 1 j == f 0 j && f 2 j == f 0 j
    || f 1 1 == s && f 0 0 == s && f 2 2 == s
    || f 1 1 == s && f 0 2 == s && f 2 0 == s
    where   f ii jj = b U.! ind ii jj
            !s = f i j

