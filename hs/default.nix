{ pkgs ? import <nixpkgs> {} }:
let drv = pkgs.haskellPackages.callCabal2nix "line-of-gaming" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

