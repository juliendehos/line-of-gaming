{-# LANGUAGE ExistentialQuantification #-}

import System.Environment (getArgs)
import System.Random (getStdGen, StdGen)
import Text.Read

import qualified Data.Map as Map

import Bot.Bot
import Bot.BotMc
import Bot.BotRandom
import Core.Params
import Game.Game
import Game.GameFind3
import Game.GameTictactoe

-------------------------------------------------------------------------------
-- bots
-------------------------------------------------------------------------------

data AnyBot = forall b. (Bot b) => AnyBot b 

instance Bot AnyBot where
    getRefParams (AnyBot b) = getRefParams b
    newBot = error "AnyBot doesn't implement newBot"
    chooseAction g (AnyBot b) = AnyBot <$> chooseAction g b

botFactory :: Map.Map String (Params -> StdGen -> AnyBot)
botFactory = Map.fromList
    [ ("mc",     \p g -> AnyBot (newBot p g :: BotMc))
    , ("random", \p g -> AnyBot (newBot p g :: BotRandom))
    ]

-------------------------------------------------------------------------------
-- games
-------------------------------------------------------------------------------

data AnyGame = forall g. (Game g) => AnyGame g 

instance Game AnyGame where
    newGame = error "AnyGame doesn't implement newGame"
    applyAction i (AnyGame g0) = AnyGame $ applyAction i g0
    getNbLegalActions (AnyGame g) = getNbLegalActions g
    getStatus (AnyGame g) = getStatus g

gameFactory :: Map.Map String AnyGame
gameFactory = Map.fromList
    [ ("find3",     AnyGame (newGame :: GameFind3))
    , ("tictactoe", AnyGame (newGame :: GameTictactoe))
    ]

-------------------------------------------------------------------------------
-- main
-------------------------------------------------------------------------------

data Model = Model
    { _game      :: AnyGame
    , _bot0      :: AnyBot
    , _bot1      :: AnyBot
    , _nRemRuns  :: Int
    , _nWins0    :: Int
    , _nWins1    :: Int
    , _nTies     :: Int
    , _swapBots  :: Bool
    }

-- TODO refactor
playOne :: AnyGame -> AnyBot -> AnyBot -> (AnyBot, AnyBot, Status)
playOne game botA botB =
    case getStatus game of
        Play0 -> let (k, b) = chooseAction game botA
                 in playOne (applyAction k game) b botB
        Play1 -> let (k, b) = chooseAction game botB
                 in playOne (applyAction k game) botA b
        s -> (botA, botB, s)

play :: Model -> Model
play m@(Model game bot0 bot1 nremruns nwins0 nwins1 nties swapbots) =
    if nremruns == 0
    then m
    else let (botA, botB) = if swapbots then (bot1, bot0) else (bot0, bot1)
             m' = m { _nRemRuns = nremruns-1, _swapBots = not swapbots }
         in case (playOne game botA botB, swapbots) of
            ((ba, bb, Win0), False) -> play m' { _bot0=ba, _bot1=bb, _nWins0=nwins0+1 }
            ((ba, bb, Win1), False) -> play m' { _bot0=ba, _bot1=bb, _nWins1=nwins1+1 }
            ((ba, bb, Tie ), False) -> play m' { _bot0=ba, _bot1=bb, _nTies=nties+1   }
            ((ba, bb, Win0), True ) -> play m' { _bot0=bb, _bot1=ba, _nWins1=nwins1+1 }
            ((ba, bb, Win1), True ) -> play m' { _bot0=bb, _bot1=ba, _nWins0=nwins0+1 }
            ((ba, bb, Tie ), True ) -> play m' { _bot0=bb, _bot1=ba, _nTies=nties+1   }
            _ -> error "play: playOne status"

main :: IO ()
main = do
    args <- getArgs
    gen <- getStdGen
    case args of
        [nRunsStr, gameName, bot0Name, bot0Params, bot1Name, bot1Params] -> do
            let nRunsM = (\x -> if even x then x else x+1) <$> readMaybe nRunsStr
                mg = Map.lookup gameName gameFactory
                mb0 = Map.lookup bot0Name botFactory
                mb1 = Map.lookup bot1Name botFactory
            case (nRunsM, mg, mb0, mb1) of
                (Just nRuns, Just game, Just newBot0, Just newBot1) -> do
                    let pbot0 = parseParams bot0Params
                        bot0 = newBot0 pbot0 gen
                        -- pbot0' = mergeParams (getRefParams bot0) pbot0
                        pbot1 = parseParams bot1Params
                        bot1 = newBot1 pbot1 gen
                        -- pbot1' = mergeParams (getRefParams bot1) pbot1
                        model = Model game bot0 bot1 nRuns 0 0 0 False
                        model' = play model
                    putStrLn $ "nruns;game;"
                        ++ "bot0_" ++ bot0Name ++ ";" -- TODO pbot0
                        ++ "bot1_" ++ bot1Name ++ ";" -- TODO pbot1
                        ++ "nbot0;nbot1;nties";
                    putStrLn $ show nRuns ++ ";"
                        ++ bot0Name ++ ";" -- TODO pbot0
                        ++ bot1Name ++ ";" -- TODO pbot1
                        ++ show (_nWins0 model') ++ ";"
                        ++ show (_nWins1 model') ++ ";"
                        ++ show (_nTies model')
                _ -> putStrLn "invalid arguments"
        _ -> do
            putStrLn "args: <nruns> <game> <bot1> <pbot1> <bot2> <pbot2>"
            putStrLn "example: 100 tictactoe random '' mc 'nSims=1000'"
            putStr "game:"
            putStrLn $ concatMap ("\n  - " ++) $ Map.keys gameFactory
            putStr "bot:"
            putStrLn $ concatMap (fmtRefBot gen) (Map.toList botFactory)

fmtRefBot :: StdGen -> (String, Params -> StdGen -> AnyBot) -> String
fmtRefBot gen (n, f) = 
    let pStr = showParams $ getRefParams $ f emptyParams gen
    in "\n  - " ++ n ++ " '" ++ pStr ++ "'"



{-

import Options.Applicative
import System.Random

-- import qualified Data.Map as Map

import Bot.Bot
-- import Bot.BotRandom
import Bot.BotMc
import Core.Params

data Args = Args
    { _game :: String
    , _bot1 :: String
    , _bot2 :: String
    } deriving (Show)

argsP :: Parser Args
argsP = Args
      <$> argument str (metavar "GAME")
      <*> argument str (metavar "BOT1")
      <*> argument str (metavar "BOT2")

myinfo :: ParserInfo Args
myinfo = info (argsP <**> helper)
              (fullDesc <> header "This is line-of-gaming!")

main :: IO ()
main = do
    putStrLn "TODO"
    execParser myinfo >>= print
    gen <- getStdGen
    let b = newBot (parseParams "ieu") gen :: BotMc
    putStrLn $ showParams $ getRefParams b
    print $ _kWin b

-}

