{-# LANGUAGE BangPatterns #-}

import System.Environment (getArgs)
import System.Random (getStdGen, StdGen)

import qualified Data.Map as Map

import Bot.Bot
import Bot.BotMc
import Bot.BotRandom
import Core.Params
import Game.Game
import Game.GameFind3
import Game.GameTictactoe
import VBot
import VGame

avgameFactory :: Map.Map String AnyVGame
avgameFactory = Map.fromList
    [ ("find3",     AnyVGame (newGame :: GameFind3))
    , ("tictactoe", AnyVGame (newGame :: GameTictactoe))
    ]

avbotFactory :: Map.Map String (Params -> StdGen -> AnyVBot)
avbotFactory = Map.fromList
    [ ("human",  \p g -> AnyVBot (newBot p g :: BotHuman))
    , ("mc",     \p g -> AnyVBot (newBot p g :: BotMc))
    , ("random", \p g -> AnyVBot (newBot p g :: BotRandom))
    ]

-- TODO ? play :: StateT (AnyVGame, AnyVBot, AnyVBot) IO ()
play :: AnyVGame -> AnyVBot -> AnyVBot -> IO ()
play game bot0 bot1 = do
    displayVGame game
    case getStatus game of
        Play0 -> do
            (k, bot0') <- runVBot game bot0
            play (applyAction k game) bot0' bot1
        Play1 -> do
            (k, bot1') <- runVBot game bot1
            play (applyAction k game) bot0 bot1'
        _ -> return ()

-- TODO args -> optparse ?
main :: IO ()
main = do
    args <- getArgs
    gen <- getStdGen
    case args of
        [gameName, bot0Name, bot0Params, bot1Name, bot1Params] -> do
            let mg = Map.lookup gameName avgameFactory
                mb0 = Map.lookup bot0Name avbotFactory
                mb1 = Map.lookup bot1Name avbotFactory
            case (mg, mb0, mb1) of
                (Just game, Just newBot0, Just newBot1) -> do
                    let !bot0 = newBot0 (parseParams bot0Params) gen
                        !bot1 = newBot1 (parseParams bot1Params) gen
                    play game bot0 bot1
                _ -> putStrLn "invalid arguments"
        _ -> do
            putStrLn "args: <game> <bot0> <pbot0> <bot1> <pbot1>"
            putStrLn "example: tictactoe human '' mc 'nSims=1000'"
            putStr "game:"
            putStrLn $ concatMap ("\n  - " ++) $ Map.keys avgameFactory
            putStr "bot:"
            putStrLn $ concatMap (fmtRefBot gen) (Map.toList avbotFactory)

fmtRefBot :: StdGen -> (String, Params -> StdGen -> AnyVBot) -> String
fmtRefBot gen (n, f) = 
    let pStr = showParams $ getRefParams $ f emptyParams gen
    in "\n  - " ++ n ++ " '" ++ pStr ++ "'"

