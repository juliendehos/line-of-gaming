{-# LANGUAGE ExistentialQuantification #-}

module VBot where

import System.IO

import Bot.Bot
import Bot.BotMc
import Bot.BotRandom
import Core.Params
import Game.Game
import VGame

-------------------------------------------------------------------------------
-- VBot
-------------------------------------------------------------------------------

class VBot vb where
    runVBot :: (VGame vg, Game vg) => vg -> vb -> IO (Int, vb)

-------------------------------------------------------------------------------
-- AnyVBot
-------------------------------------------------------------------------------

data AnyVBot = forall vb. (VBot vb, Bot vb) => AnyVBot vb 

instance VBot AnyVBot where
    runVBot vg (AnyVBot vb) = fmap AnyVBot <$> runVBot vg vb

instance Bot AnyVBot where
    getRefParams (AnyVBot b) = getRefParams b
    newBot = error "AnyVBot doesn't implement newBot"
    chooseAction g (AnyVBot b) = AnyVBot <$> chooseAction g b

-------------------------------------------------------------------------------
-- BotHuman
-------------------------------------------------------------------------------

data BotHuman = BotHuman

instance VBot BotHuman where
    runVBot vg b = do
        putStr "action? "
        hFlush stdout
        line <- getLine
        case parseVGame vg line of
            Nothing -> putStrLn "invalid input" >> runVBot vg b
            Just k -> return (k, b)

instance Bot BotHuman where
    getRefParams _ = emptyParams
    newBot _ _ = BotHuman
    chooseAction _vg = error "BotHuman doesn't implement chooseAction"

-------------------------------------------------------------------------------
-- other bots
-------------------------------------------------------------------------------

instance VBot BotRandom where
    runVBot g b = return $ chooseAction g b

instance VBot BotMc where
    runVBot g b = return $ chooseAction g b

