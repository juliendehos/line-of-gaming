{-# LANGUAGE ExistentialQuantification #-}

module VGame where

import Control.Applicative
import Control.Monad
import Data.Char
import Data.List
import Text.Read

import qualified Data.Vector.Unboxed as U

import Game.Game
import Game.GameFind3
import Game.GameTictactoe

-------------------------------------------------------------------------------
-- VGame
-------------------------------------------------------------------------------

class VGame vg where
    displayVGame :: vg -> IO ()
    parseVGame :: vg -> String -> Maybe Int

-------------------------------------------------------------------------------
-- AnyVGame
-------------------------------------------------------------------------------

data AnyVGame = forall vg. (VGame vg, Game vg) => AnyVGame vg 

instance VGame AnyVGame where
    displayVGame (AnyVGame vg) = displayVGame vg
    parseVGame (AnyVGame vg) = parseVGame vg

instance Game AnyVGame where
    newGame = error "AnyVGame doesn't implement newGame"
    applyAction i (AnyVGame vg0) = AnyVGame $ applyAction i vg0
    getNbLegalActions (AnyVGame vg) = getNbLegalActions vg
    getStatus (AnyVGame vg) = getStatus vg

-------------------------------------------------------------------------------
-- GameFind3
-------------------------------------------------------------------------------

instance VGame GameFind3 where
    displayVGame vg = do
        putStr "\nhistory: "
        print $ _history vg
        putStr "status: "
        print $ getStatus vg

    parseVGame _v str = do
        i <- readMaybe str 
        if i>=0 && i<=9 then return i else empty

-------------------------------------------------------------------------------
-- GameTictactoe
-------------------------------------------------------------------------------

instance VGame GameTictactoe where
    displayVGame vg = do
        let fmt c = " " ++ [c] ++ " "
        putStr "\n   "
        putStrLn $ concatMap (\i -> " " ++ show i ++ " ") [1..3::Int]
        putStr "   "
        putStrLn $ replicate 9 '-'
        forM_ [0..2] $ \i -> do
            putStr $ chr (97 + i) : " |"
            forM_ [0..2] $ \j -> 
                putStr $ fmt (_board vg U.! ind i j)
            putStrLn ""
        putStr "status: "
        print $ getStatus vg

    parseVGame vg [ii,jj] = 
        let i = ord ii - 97
            j = ord jj - 49
        in elemIndex (Action i j) (_legalActions vg)
    parseVGame _ _ = Nothing

