module Constants where

viewWidth, viewHeight :: Int
viewWidth = 400
viewHeight = 400

viewWidthF, viewHeightF :: Float
viewWidthF = fromIntegral viewWidth
viewHeightF = fromIntegral viewHeight

viewWidthF2, viewHeightF2 :: Float
viewWidthF2 = viewWidthF / 2
viewHeightF2 = viewHeightF / 2

viewWidthF3, viewHeightF3 :: Float
viewWidthF3 = viewWidthF / 3
viewHeightF3 = viewHeightF / 3

viewWidthF4, viewHeightF4 :: Float
viewWidthF4 = viewWidthF / 4
viewHeightF4 = viewHeightF / 4

