module VGameFind3 where

import qualified Graphics.Gloss as G

import Constants
import Game.GameFind3

click :: GameFind3 -> (Float, Float) -> Maybe Int
click _vg (x, y) =
    let i = floor (y + viewHeightF2) * 5 `div` viewHeight
        j = floor (x + viewWidthF2) * 2 `div` viewWidth
    in Just (i * 2 + j)

display :: GameFind3 -> G.Picture
display vg = G.Pictures
    (   [ G.Line [(viewWidthF2, 0), (viewWidthF2, viewHeightF)] ]
    ++ [ G.Line [(0, h), (viewWidthF, h)] | i<-[1..4], let h=i*viewWidthF/5 ]
    ++ [ G.Translate (w4 + viewWidthF2 * fromIntegral j) (10 + h5 * fromIntegral i)
            $ G.Scale 0.3 0.3 $ fmtColor i j $ G.Text $ show $ ind2 i j
            | i<-[0..4], j<-[0,1] ]
    )
    where   w4 = viewWidthF / 4
            h5 = viewHeightF / 5
            ind2 i j = 2*i + j
            fmtColor i j = if ind2 i j `elem` _history vg
                            then G.Color G.white
                            else G.Color G.black

