{-# LANGUAGE BangPatterns #-}

import System.Environment (getArgs)
import System.Random (getStdGen, StdGen)

import qualified Data.Map as Map
import qualified Graphics.Gloss as G
import qualified Graphics.Gloss.Interface.IO.Interact as G

import Bot.Bot
import Bot.BotMc
import Bot.BotRandom
import Constants
import Core.Params
import Game.Game
import Game.GameFind3
import Game.GameTictactoe
import VBot
import VGame

avgameFactory :: Map.Map String AnyVGame
avgameFactory = Map.fromList
    [ ("find3",     AnyVGame (newGame :: GameFind3))
    , ("tictactoe", AnyVGame (newGame :: GameTictactoe))
    ]

avbotFactory :: Map.Map String (Params -> StdGen -> AnyVBot)
avbotFactory = Map.fromList
    [ ("human",  \p g -> AnyVBot (newBot p g :: BotHuman))
    , ("mc",     \p g -> AnyVBot (newBot p g :: BotMc))
    , ("random", \p g -> AnyVBot (newBot p g :: BotRandom))
    ]

data Model = Model
    { _bot0  :: AnyVBot
    , _bot1  :: AnyVBot
    , _game  :: AnyVGame
    , _game0 :: AnyVGame
    }

-- play :: [Int] -> Model -> Model -- TODO (breakthrough...)
play :: Maybe Int -> Model -> Model
play mi m@(Model bot0 bot1 game _) =
    case getStatus game of
        Play0 -> case runVBot mi game bot0 of
            Nothing -> m
            Just (k, bot0') -> 
                play Nothing m { _game = applyAction k game, _bot0 = bot0' }
        Play1 -> case runVBot mi game bot1 of
            Nothing -> m
            Just (k, bot1') ->
                play Nothing m { _game = applyAction k game, _bot1 = bot1' }
        _ -> m

event :: G.Event -> Model -> Model
event (G.EventKey (G.MouseButton G.MiddleButton) G.Up _ _) m = 
    m { _game = _game0 m }
event (G.EventKey (G.MouseButton G.LeftButton) G.Up _ p) m =
    let game = _game m
        mi = clickVGame game p
    in play mi m
event _ m = m

display :: Model -> G.Picture
display = displayVGame . _game

main :: IO ()
main = do
    args <- getArgs
    gen <- getStdGen
    case args of
        [gameName, bot0Name, bot0Params, bot1Name, bot1Params] -> do
            let mg = Map.lookup gameName avgameFactory
                mb0 = Map.lookup bot0Name avbotFactory
                mb1 = Map.lookup bot1Name avbotFactory
            case (mg, mb0, mb1) of
                (Just game, Just newBot0, Just newBot1) ->
                    let !bot0 = newBot0 (parseParams bot0Params) gen
                        !bot1 = newBot1 (parseParams bot1Params) gen
                        model = Model bot0 bot1 game game
                        win = G.InWindow "line-of-gaming" (viewWidth,viewHeight) (0,0)
                        idle _ = id
                    in G.play win G.azure 10 model display event idle
                _ -> putStrLn "unknown game/bot"
        _ -> do
            putStrLn "args: <game> <bot0> <pbot0> <bot1> <pbot1>"
            putStrLn "example: tictactoe human '' mc 'nSims=1000'"
            putStr "game:"
            putStrLn $ concatMap ("\n  - " ++) $ Map.keys avgameFactory
            putStr "bot:"
            putStrLn $ concatMap (fmtRefBot gen) (Map.toList avbotFactory)

fmtRefBot :: StdGen -> (String, Params -> StdGen -> AnyVBot) -> String
fmtRefBot gen (n, f) = 
    let pStr = showParams $ getRefParams $ f emptyParams gen
    in "\n  - " ++ n ++ " '" ++ pStr ++ "'"

