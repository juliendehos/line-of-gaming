{-# LANGUAGE ExistentialQuantification #-}

module VGame where

import qualified Graphics.Gloss as G

import Constants
import Game.Game
import Game.GameFind3
import Game.GameTictactoe

import qualified VGameFind3
import qualified VGameTictactoe

-------------------------------------------------------------------------------
-- VGame
-------------------------------------------------------------------------------

class VGame vg where
    clickVGame :: vg -> (Float, Float) -> Maybe Int
    displayVGame :: vg -> G.Picture

-------------------------------------------------------------------------------
-- AnyVGame
-------------------------------------------------------------------------------

data AnyVGame = forall vg. (VGame vg, Game vg) => AnyVGame vg 

instance Game AnyVGame where
    newGame = newGame
    applyAction i (AnyVGame vg) = AnyVGame (applyAction i vg)
    getNbLegalActions (AnyVGame vg) = getNbLegalActions vg
    getStatus (AnyVGame vg) = getStatus vg

instance VGame AnyVGame where
    clickVGame (AnyVGame vg) = clickVGame vg

    displayVGame (AnyVGame vg) = 
        G.Translate (-viewWidthF2) (-viewHeightF2) $ G.Pictures (displayVGame vg : s)
        where s = [ G.Translate (viewWidthF2 - 40) (viewHeightF2 - 20)
                        $ G.Scale 0.3 0.3 $ G.Color G.red 
                        $ G.Text $ show $ getStatus vg
                        | not (isPlaying vg) ]
        -- change coordinate system to:
        -- +----------+
        -- |          |
        -- |          |
        -- ^          |
        -- |          |
        -- +-->-------+

-------------------------------------------------------------------------------
-- games
-------------------------------------------------------------------------------

instance VGame GameFind3 where
    clickVGame = VGameFind3.click
    displayVGame = VGameFind3.display

instance VGame GameTictactoe where
    clickVGame = VGameTictactoe.click
    displayVGame = VGameTictactoe.display

