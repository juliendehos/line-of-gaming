module VGameTictactoe where

import Data.List (elemIndex)
import qualified Data.Vector.Unboxed as U
import qualified Graphics.Gloss as G

import Constants
import Game.GameTictactoe

click :: GameTictactoe -> (Float, Float) -> Maybe Int
click vg (x, y) =
    let i = floor (y + viewHeightF2) * 3 `div` viewHeight
        j = floor (x + viewWidthF2) * 3 `div` viewWidth
    in Action i j `elemIndex` _legalActions vg

display :: GameTictactoe -> G.Picture
display vg = G.Pictures
    ([ G.Line [(viewWidthF3, 0), (viewWidthF3, viewHeightF)]
     , G.Line [(2*viewWidthF3, 0), (2*viewWidthF3, viewHeightF)]
     , G.Line [(0, viewHeightF3), (viewWidthF, viewHeightF3)]
     , G.Line [(0, 2*viewHeightF3), (viewWidthF, 2*viewHeightF3)]
    ] ++ boardPics)
    where   fmtPic i j b = 
                let x = fromIntegral j * viewWidthF3 +  viewWidthF3 / 2
                    y = fromIntegral i * viewHeightF3 +  viewHeightF3 / 2
                    c = if b == 'O' then G.black else G.white
                in G.Translate x y $ G.color c $ G.thickCircle 10 viewWidthF4
            boardPics = [ fmtPic i j c | i<-[0..2], j<-[0..2]
                                       , let c = _board vg U.! ind i j
                                       , c /= '.' ]
 
