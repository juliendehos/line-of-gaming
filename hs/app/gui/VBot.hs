{-# LANGUAGE ExistentialQuantification #-}

module VBot where

import Bot.Bot
import Bot.BotMc
import Bot.BotRandom
import Core.Params
import Game.Game

-------------------------------------------------------------------------------
-- VBot
-------------------------------------------------------------------------------

class VBot vb where
    runVBot :: Game g => Maybe Int -> g -> vb -> Maybe (Int, vb)

-------------------------------------------------------------------------------
-- AnyVBot
-------------------------------------------------------------------------------

data AnyVBot = forall vb. (VBot vb, Bot vb) => AnyVBot vb 

instance VBot AnyVBot where
    runVBot mi g (AnyVBot vb) = 
        case runVBot mi g vb of
            Nothing -> Nothing
            Just (k, vb') -> Just (k, AnyVBot vb')

instance Bot AnyVBot where
    getRefParams (AnyVBot b) = getRefParams b
    newBot = error "AnyVBot doesn't implement newBot"
    chooseAction g (AnyVBot b) = AnyVBot <$> chooseAction g b

-------------------------------------------------------------------------------
-- BotHuman
-------------------------------------------------------------------------------

data BotHuman = BotHuman

instance VBot BotHuman where
    runVBot Nothing _g _b = Nothing
    runVBot (Just k) _g b = Just (k, b)

instance Bot BotHuman where
    getRefParams _ = emptyParams
    newBot _ _ = BotHuman
    chooseAction _g = error "BotHuman doesn't implement chooseAction"

-------------------------------------------------------------------------------
-- bots
-------------------------------------------------------------------------------

instance VBot BotRandom where
    runVBot _k g b = Just $ chooseAction g b

instance VBot BotMc where
    runVBot _k g b = Just $ chooseAction g b

