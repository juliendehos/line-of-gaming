module Bot.BotMcState where
-- module Bot.BotMc where

import Control.Monad.State

import Bot.Bot
import Bot.BotRandom
import Core.Params
import Game.Game

data BotMc = BotMc
    { _kWin  :: Double
    , _kTie  :: Double
    , _kLose :: Double
    , _nSims :: Int
    , _bot   :: BotRandom
    }

botMcParams :: Params
botMcParams = fromList
    [ ("kWin", 1)
    , ("kTie", 0.75)
    , ("kLose", 0)
    , ("nSims", 10000)
    ]

instance Bot BotMc where
    getRefParams _ = botMcParams

    newBot params gen =
        let params1 = mergeParams botMcParams params
            kWin = getParam params1 "kWin"
            kTie = getParam params1 "kTie"
            kLose = getParam params1 "kLose"
            nSims = floor $ getParam params1 "nSims"
        in BotMc kWin kTie kLose nSims (BotRandom gen)

    chooseAction game botMc = 
        let nActions = getNbLegalActions game
            nRollouts = max 1 (_nSims botMc `div` nActions)
            evalsM = mapM (evalAction game botMc nRollouts) [0 .. nActions - 1]
            (evals, bot') = runState evalsM (_bot botMc)
            kBest = snd $ maximum $ zip evals [0..]
        in (kBest, botMc { _bot = bot' })

evalAction :: Game g => g -> BotMc -> Int -> Int -> State BotRandom Double
evalAction g0 botMc nRollouts k = do
    let s0 = getStatus g0
        g1 = applyAction k g0
        rollouts = mapM (\_ -> rollout g1) [1..nRollouts]
    sum . map (evalStatus botMc s0) <$> rollouts

rollout :: Game g => g -> State BotRandom Status
rollout g0
    | isPlaying g0 = do
        b0 <- get
        let (k, b1) = chooseAction g0 b0
            g1 = applyAction k g0
        put b1
        rollout g1
    | otherwise = return $ getStatus g0

evalStatus :: BotMc -> Status -> Status -> Double
evalStatus botMc Play0 Win0 = _kWin botMc
evalStatus botMc Play0 Win1 = _kLose botMc
evalStatus botMc Play1 Win0 = _kLose botMc
evalStatus botMc Play1 Win1 = _kWin botMc
evalStatus botMc _     Tie  = _kTie botMc
evalStatus _     _     _    = error "BotMc: evalStatus: game not finished"

