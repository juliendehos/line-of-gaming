module Bot.BotMcSpec (main, spec) where

import Control.Monad.State
import System.Random
import Test.Hspec

import Bot.Bot
import Bot.BotMc
import Core.Params
import Game.Game
import Game.GameFind3

main :: IO ()
main = hspec spec

choose :: Game g => g -> State BotMc Int
choose game = do
    bot <- get
    let (k, bot') = chooseAction game bot
    put bot'
    return k

spec :: Spec
spec = do
    gen <- runIO getStdGen
    let b0 = newBot emptyParams gen :: BotMc
    let g0 = newGame :: GameFind3
    let ns = evalState (replicateM 100 $ choose g0) b0

    describe "find 3" $ 
        it "0" $ all (==3) ns

