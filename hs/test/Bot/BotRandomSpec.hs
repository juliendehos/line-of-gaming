module Bot.BotRandomSpec (main, spec) where

import Control.Monad.State
import System.Random
import Test.Hspec

import Bot.Bot
import Bot.BotRandom
import Core.Params
import Game.Game
import Game.GameFind3

main :: IO ()
main = hspec spec

choose :: Game g => g -> State BotRandom Int
choose game = do
    bot <- get
    let (k, bot') = chooseAction game bot
    put bot'
    return k

spec :: Spec
spec = do
    gen <- runIO getStdGen
    let b0 = newBot emptyParams gen :: BotRandom
    let g0 = newGame :: GameFind3
    let ns = evalState (replicateM 10000 $ choose g0) b0

    describe "average" $ do
        let m = fromIntegral (sum ns) / fromIntegral (length ns) :: Double
        it "0" $ m < 4.6
        it "1" $ m > 4.4

    describe "range" $ do
        it "0" $ all (<= 9) ns
        it "1" $ all (>= 0) ns

    describe "hist" $ do
        let h = [length (filter (== k) ns) | k<-[0..9]]
        it "0" $ all (<= 1100) h
        it "1" $ all (>=  900) h

