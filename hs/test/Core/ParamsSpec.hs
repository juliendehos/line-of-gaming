module Core.ParamsSpec (main, spec) where

import Test.Hspec

import Core.Params

main :: IO ()
main = hspec spec

-- TODO test errors:
-- "kWin=0.9,kLose=toto"
-- "kWin=0.9,kLose"
-- "kWin=0.9,kLos=0"

spec :: Spec
spec = do

    let params0 = fromList [("kWin",1),("kLose",0)]

    describe "parseParams kWin kLose" $
        it "1" $
            parseParams "kWin=0.9,kLose=-1"
            `shouldBe`
            fromList [("kWin",0.9),("kLose",-1)]

    describe "parseParams kLose" $
        it "1" $
            parseParams "kLose=0.1"
            `shouldBe`
            fromList [("kLose",0.1)]

    describe "parseParams empty" $
        it "1" $
            parseParams ""
            `shouldBe`
            emptyParams

    describe "mergeParams kWin kLose" $
        it "1" $
            mergeParams params0 (fromList [("kWin", 0.9), ("kLose",0.1)])
            `shouldBe`
            fromList [("kWin",0.9),("kLose",0.1)]

    describe "mergeParams kWin" $
        it "1" $
            mergeParams params0 (fromList [("kWin", 0.9)])
            `shouldBe`
            fromList [("kWin",0.9),("kLose",0)]

    describe "getParam" $ do
        it "1" $ getParam params0 "kWin" `shouldBe` 1
        it "2" $ getParam params0 "kLose" `shouldBe` 0

    describe "showParams" $
        it "1" $ showParams params0 `shouldBe` "kLose=0.0,kWin=1.0"

