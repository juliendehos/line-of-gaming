{-# LANGUAGE ExistentialQuantification #-}

module Game.GameTictactoeSpec (main, spec) where

import Test.Hspec

import qualified Data.Vector.Unboxed as U

import Game.Game
import Game.GameTictactoe

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "init" $ do
        let g = newGame :: GameTictactoe
        it "new game" $ 0 `shouldBe` (0 :: Int)
        it "status" $ getStatus g `shouldBe` Play0
        it "is playing" $ isPlaying g `shouldBe` True
        it "nb actions" $ getNbLegalActions g `shouldBe` 9
        it "legal actions" $ _legalActions g `shouldBe`
            [Action 0 0, Action 0 1, Action 0 2
            ,Action 1 0, Action 1 1, Action 1 2
            ,Action 2 0, Action 2 1, Action 2 2]
        it "board" $ _board g `shouldBe` U.fromList 
                ['.', '.', '.'
                ,'.', '.', '.'
                ,'.', '.', '.']

    describe "apply 4" $ do
        let g = applyAction 4 (newGame :: GameTictactoe)
        it "status" $ getStatus g `shouldBe` Play1
        it "nb actions" $ getNbLegalActions g `shouldBe` 8
        it "legal actions" $ _legalActions g `shouldBe`
            [Action 0 0, Action 0 1, Action 0 2
            ,Action 1 0,             Action 1 2
            ,Action 2 0, Action 2 1, Action 2 2]
        it "board" $ _board g `shouldBe` U.fromList 
                ['.', '.', '.'
                ,'.', 'O', '.'
                ,'.', '.', '.']

    describe "apply 2 3" $ do
        let g = foldr applyAction (newGame :: GameTictactoe) [3,2]
        it "status" $ getStatus g `shouldBe` Play0
        it "nb actions" $ getNbLegalActions g `shouldBe` 7
        it "legal actions" $ _legalActions g `shouldBe`
            [Action 0 0, Action 0 1            
            ,Action 1 0,             Action 1 2
            ,Action 2 0, Action 2 1, Action 2 2]
        it "board" $ _board g `shouldBe` U.fromList 
                ['.', '.', 'O'
                ,'.', 'X', '.'
                ,'.', '.', '.']

    describe "apply 0 0 2 0 4" $ do
        let g = foldr applyAction (newGame :: GameTictactoe) [4,0,2,0,0]
        it "status" $ getStatus g `shouldBe` Win0
        it "nb actions" $ getNbLegalActions g `shouldBe` 4
        it "legal actions" $ _legalActions g `shouldBe`
            [                                   
             Action 1 0,             Action 1 2
            ,Action 2 0, Action 2 1            ]
        it "board" $ _board g `shouldBe` U.fromList 
                ['O', 'X', 'X'
                ,'.', 'O', '.'
                ,'.', '.', 'O']

    describe "apply 3 0 2 0 4 0" $ do
        let g = foldr applyAction (newGame :: GameTictactoe) [0,4,0,2,0,3]
        it "status" $ getStatus g `shouldBe` Win1
        it "nb actions" $ getNbLegalActions g `shouldBe` 3
        it "legal actions" $ _legalActions g `shouldBe`
            [                                    
                                     Action 1 2
            ,Action 2 0, Action 2 1            ]
        it "board" $ _board g `shouldBe` U.fromList 
                ['X', 'X', 'X'
                ,'O', 'O', '.'
                ,'.', '.', 'O']

    describe "apply 0 0 2 0 1 0 0 1 0" $ do
        let g = foldr applyAction (newGame :: GameTictactoe) [0,1,0,0,1,0,2,0,0]
        it "status" $ getStatus g `shouldBe` Tie
        it "nb actions" $ getNbLegalActions g `shouldBe` 0
        it "legal actions" $ _legalActions g `shouldBe` []
        it "board" $ _board g `shouldBe` U.fromList 
                ['O', 'X', 'X'
                ,'X', 'O', 'O'
                ,'O', 'O', 'X']

