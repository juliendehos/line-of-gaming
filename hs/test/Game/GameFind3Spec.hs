module Game.GameFind3Spec (main, spec) where

import Test.Hspec

import Game.Game
import Game.GameFind3

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "init" $ do
        let g = newGame :: GameFind3
        it "new game" $ g `shouldBe` GameFind3 [] Play0
        it "status" $ getStatus g `shouldBe` Play0
        it "is playing" $ isPlaying g `shouldBe` True
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $ _history g `shouldBe` []

    describe "apply 5" $ do
        let g = applyAction 5 (newGame :: GameFind3)
        it "status" $ getStatus g `shouldBe` Play1
        it "is playing" $ isPlaying g `shouldBe` True
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $  _history g `shouldBe` [5]

    describe "apply 2 3" $ do
        let g = foldr applyAction (newGame :: GameFind3) [3,2] 
        it "status" $ getStatus g `shouldBe` Win1
        it "is playing" $ isPlaying g `shouldBe` False
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $  _history g `shouldBe` [3,2]

    describe "apply 2 3 4" $ do
        let g = foldr applyAction (newGame :: GameFind3) [4,3,2] 
        it "status" $ getStatus g `shouldBe` Win1
        it "is playing" $ isPlaying g `shouldBe` False
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $  _history g `shouldBe` [3,2]

    describe "apply 9 8 7 6" $ do
        let g = foldr applyAction (newGame :: GameFind3) [6..9] 
        it "status" $ getStatus g `shouldBe` Tie
        it "is playing" $ isPlaying g `shouldBe` False
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $  _history g `shouldBe` [6..9]

    describe "apply 9 8 7 6 5" $ do
        let g = foldr applyAction (newGame :: GameFind3) [5..9] 
        it "status" $ getStatus g `shouldBe` Tie
        it "is playing" $ isPlaying g `shouldBe` False
        it "nb actions" $ getNbLegalActions g `shouldBe` 10
        it "history" $  _history g `shouldBe` [6..9]

