-------------------------------------------------------------------------------
-- runghc proto2b.hs Game1 BotRandom
--
-- ExistentialQuantification
--
-- No public action. We just use the indices of the actions inside the game.
-- No view. Deriving instance Show.
-------------------------------------------------------------------------------

{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE StandaloneDeriving #-}

import System.Environment (getArgs)
import Text.Read (readMaybe)
import System.Random (randomRIO)

-------------------------------------------------------------------------------
-- AnyGame (Game, ViewCli).
-------------------------------------------------------------------------------

data AnyGame = forall g. (ViewCli g, Game g) => AnyGame g 

instance Game AnyGame where
    isTerminated (AnyGame g) = isTerminated g
    nbLegalActions (AnyGame g) = nbLegalActions g
    applyAction i (AnyGame g) = AnyGame $ applyAction i g

class Game g where
    isTerminated :: g -> Bool
    nbLegalActions :: g -> Int
    applyAction :: Int -> g -> g

class ViewCli g where
    viewGame :: g -> IO ()

instance ViewCli AnyGame where
    viewGame (AnyGame g) = viewGame g

instance ViewCli Game1 where
    viewGame _g1 = putStrLn "Game1"

instance ViewCli Game2 where
    viewGame _g2 = putStrLn "Game2"

-------------------------------------------------------------------------------
-- Bot.
-------------------------------------------------------------------------------

class Bot b where
    chooseAction :: Game g => g -> b -> IO (Int, b)

data AnyBot = forall b. Bot b => AnyBot b

instance Bot AnyBot where
    chooseAction g (AnyBot b) = do
        (i, b') <- chooseAction g b
        return (i, AnyBot b')

-------------------------------------------------------------------------------
-- Game1.
-------------------------------------------------------------------------------

newtype Action1 = Action1 { pos1 :: Int } deriving Show

data Game1 = Game1
    { target1 :: Int
    , last1 :: Action1
    } deriving Show

instance Game Game1 where
    isTerminated (Game1 n l) = n == pos1 l
    nbLegalActions _ = 5
    applyAction i g = g { last1 = Action1 i }

newGame1 :: Game1
newGame1 = Game1 1 (Action1 (-1))

-------------------------------------------------------------------------------
-- Game2.
-------------------------------------------------------------------------------

newtype Action2 = Action2 { pos2 :: Int } deriving Show

data Game2 = Game2
    { target2 :: Int
    , last2 :: Action2
    } deriving Show

instance Game Game2 where
    isTerminated (Game2 n l) = n == pos2 l
    nbLegalActions _ = 6
    applyAction i g = g { last2 = Action2 i }

newGame2 :: Game2
newGame2 = Game2 2 (Action2 (-1))

-------------------------------------------------------------------------------
-- BotHuman.
-------------------------------------------------------------------------------

data BotHuman = BotHuman

instance Bot BotHuman where
    chooseAction g b = do
        line <- getLine
        case readMaybe line of
            Nothing -> putStrLn "bad input" >> chooseAction g b
            Just iAction -> if iAction < 0 || iAction >= nbLegalActions g
                            then putStrLn "bad index" >> chooseAction g b
                            else return (iAction, b)

-------------------------------------------------------------------------------
-- BotRandom
-------------------------------------------------------------------------------

data BotRandom = BotRandom

instance Bot BotRandom where
    chooseAction g b = do
        iAction <- randomRIO (0, nbLegalActions g - 1)
        return (iAction, b)

-------------------------------------------------------------------------------
-- Factories.
-------------------------------------------------------------------------------

gameFactory :: String -> Maybe AnyGame
gameFactory "Game1" = Just $ AnyGame newGame1
gameFactory "Game2" = Just $ AnyGame newGame2
gameFactory _ = Nothing

botFactory :: String -> Maybe AnyBot
botFactory "BotHuman" = Just $ AnyBot BotHuman
botFactory "BotRandom" = Just $ AnyBot BotRandom
botFactory _ = Nothing

-------------------------------------------------------------------------------
-- Main program.
-------------------------------------------------------------------------------

play :: AnyGame -> AnyBot -> IO AnyGame
play g b = do
    let nbActions = nbLegalActions g
    viewGame g
    putStrLn $ unwords $ show <$> [0 .. nbActions - 1]
    (iAction, b') <- chooseAction g b
    putStrLn $ "-> " <> show iAction
    if iAction < 0 || iAction >= nbActions
    then putStrLn "bad index" >> play g b'
    else let g' = applyAction iAction g
         in if isTerminated g' then return g' else play g' b'

main :: IO ()
main = do
    args <- getArgs
    case args of
        [gameName, botName] -> case (gameFactory gameName, botFactory botName) of
            (Just g, Just b) -> play g b >>= viewGame
            _ -> putStrLn "unknown game/bot"
        _ -> putStrLn "args: <game> <bot>"

