-------------------------------------------------------------------------------
-- runghc proto1a.hs Game1 BotRandom
--
-- ADT
--
-- No public action. We just use the indices of the actions inside the game.
-- No view. Deriving instance Show.
-------------------------------------------------------------------------------

import System.Environment (getArgs)
import Text.Read (readMaybe)
import System.Random (randomRIO)

-------------------------------------------------------------------------------
-- AllGames.
-------------------------------------------------------------------------------

data AllGames
    = AGame1 Game1
    | AGame2 Game2
    deriving Show

isTerminated :: AllGames -> Bool
isTerminated (AGame1 g1) = isTerminatedGame1 g1
isTerminated (AGame2 g2) = isTerminatedGame2 g2

nbLegalActions :: AllGames -> Int
nbLegalActions (AGame1 g1) = nbLegalActionsGame1 g1
nbLegalActions (AGame2 g2) = nbLegalActionsGame2 g2

applyAction :: Int -> AllGames -> AllGames
applyAction i (AGame1 g1) = AGame1 (applyActionGame1 i g1)
applyAction i (AGame2 g2) = AGame2 (applyActionGame2 i g2)

-------------------------------------------------------------------------------
-- Game1.
-------------------------------------------------------------------------------

newtype Action1 = Action1 { pos1 :: Int } deriving Show

data Game1 = Game1
    { target1 :: Int
    , last1 :: Action1
    } deriving Show

isTerminatedGame1 :: Game1 -> Bool
isTerminatedGame1 (Game1 n l) = n == pos1 l

nbLegalActionsGame1 :: Game1 -> Int
nbLegalActionsGame1 _ = 5

applyActionGame1 :: Int -> Game1 -> Game1
applyActionGame1 i g = g { last1 = Action1 i }

newGame1 :: Game1
newGame1 = Game1 1 (Action1 (-1))

-------------------------------------------------------------------------------
-- Game2.
-------------------------------------------------------------------------------

newtype Action2 = Action2 { pos2 :: Int } deriving Show

data Game2 = Game2
    { target2 :: Int
    , last2 :: Action2
    } deriving Show

isTerminatedGame2 :: Game2 -> Bool
isTerminatedGame2 (Game2 n l) = n == pos2 l

nbLegalActionsGame2 :: Game2 -> Int
nbLegalActionsGame2 _ = 6

applyActionGame2 :: Int -> Game2 -> Game2
applyActionGame2 i g = g { last2 = Action2 i }

newGame2 :: Game2
newGame2 = Game2 2 (Action2 (-1))

-------------------------------------------------------------------------------
-- AllBots.
-------------------------------------------------------------------------------

data AllBots
    = ABotHuman BotHuman
    | ABotRandom BotRandom

chooseAction :: AllGames -> AllBots -> IO (Int, AllBots)
chooseAction g (ABotHuman b) = chooseActionBotHuman g b
chooseAction g (ABotRandom b) = chooseActionBotRandom g b

-------------------------------------------------------------------------------
-- BotHuman.
-------------------------------------------------------------------------------

data BotHuman = BotHuman

chooseActionBotHuman :: AllGames -> BotHuman -> IO (Int, AllBots)
chooseActionBotHuman g b = do
    line <- getLine
    case readMaybe line of
        Nothing -> putStrLn "bad input" >> chooseActionBotHuman g b
        Just iAction -> if iAction < 0 || iAction >= nbLegalActions g
                        then putStrLn "bad index" >> chooseActionBotHuman g b
                        else return (iAction, ABotHuman b)

-------------------------------------------------------------------------------
-- BotRandom
-------------------------------------------------------------------------------

data BotRandom = BotRandom

chooseActionBotRandom :: AllGames -> BotRandom -> IO (Int, AllBots)
chooseActionBotRandom g b = do
    iAction <- randomRIO (0, nbLegalActions g - 1)
    return (iAction, ABotRandom b)

-------------------------------------------------------------------------------
-- Factories.
-------------------------------------------------------------------------------

gameFactory :: String -> Maybe AllGames
gameFactory "Game1" = Just $ AGame1 newGame1
gameFactory "Game2" = Just $ AGame2 newGame2
gameFactory _ = Nothing

botFactory :: String -> Maybe AllBots
botFactory "BotHuman" = Just $ ABotHuman BotHuman
botFactory "BotRandom" = Just $ ABotRandom BotRandom
botFactory _ = Nothing

-------------------------------------------------------------------------------
-- Main program.
-------------------------------------------------------------------------------

play :: AllGames -> AllBots -> IO AllGames
play g b = do
    let nbActions = nbLegalActions g
    print g
    putStrLn $ unwords $ show <$> [0 .. nbActions - 1]
    (iAction, b') <- chooseAction g b
    putStrLn $ "-> " <> show iAction
    if iAction < 0 || iAction >= nbActions
    then putStrLn "bad index" >> play g b'
    else let g' = applyAction iAction g
         in if isTerminated g' then return g' else play g' b'

main :: IO ()
main = do
    args <- getArgs
    case args of
        [gameName, botName] -> case (gameFactory gameName, botFactory botName) of
            (Just g, Just b) -> play g b >>= print
            _ -> putStrLn "unknown game/bot"
        _ -> putStrLn "args: <game> <bot>"

