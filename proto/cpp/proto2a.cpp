///////////////////////////////////////////////////////////////////////////////
// runcpp proto2a.cpp Game1 BotRandom
//
// - Public action using index in legal actions.
// - No view. The method show() returns a string representing the game.
///////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <sstream>
#include <vector>

///////////////////////////////////////////////////////////////////////////////
// Public interfaces.
///////////////////////////////////////////////////////////////////////////////

struct Action {
    virtual std::string show() const = 0;
};

struct Game {
    virtual ~Game() = default;
    virtual bool isTerminated() const = 0;
    virtual int nbLegalActions() const = 0;
    virtual const Action * getLegalAction(int iAction) const = 0; // WARN use-after-free
    virtual void applyAction(int iAction) = 0;
    virtual std::string show() const = 0;
};

struct Bot {
    virtual int chooseAction(const Game & game) = 0;
};

///////////////////////////////////////////////////////////////////////////////
// Game1.
///////////////////////////////////////////////////////////////////////////////

struct Action1 : Action {
    int _pos1;
    Action1(int pos1) : _pos1(pos1) {}
    std::string show() const override {
        return "Action1 " + std::to_string(_pos1);
    }
};

struct Game1 : Game {
    int _target1;
    Action1 _last1;
    std::vector<Action1> _legalActions;
    Game1() : _target1(1), _last1({-1}) {
        for (int i=0; i<5; ++i)
            _legalActions.push_back({i});
    }
    bool isTerminated() const override {
        return _target1 == _last1._pos1;
    }
    int nbLegalActions() const override {
        return _legalActions.size();
    }
    const Action * getLegalAction(int iAction) const {
        assert(iAction >= 0);
        assert(iAction < nbLegalActions());
        return &(_legalActions[iAction]);
    }
    void applyAction(int iAction) override {
        if (iAction >= 0 and iAction < nbLegalActions())
            _last1 = {iAction};
    }
    std::string show() const override {
        std::ostringstream oss;
        oss << "Game1 " << _target1 << " ";
        oss << _last1.show() << std::endl;
        for (int i=0; i<nbLegalActions(); i++)
            oss << i << " ";
        oss << std::endl;
        return oss.str();
    }
};

///////////////////////////////////////////////////////////////////////////////
// Game2.
///////////////////////////////////////////////////////////////////////////////

struct Action2 : Action {
    int _pos2;
    Action2(int pos2) : _pos2(pos2) {}
    std::string show() const override {
        return "Action2 " + std::to_string(_pos2);
    }
};

struct Game2 : Game {
    int _target2;
    Action2 _last2;
    std::vector<Action2> _legalActions;
    Game2() : _target2(2), _last2({-1}) {
        for (int i=0; i<6; ++i)
            _legalActions.push_back({i});
    }
    bool isTerminated() const override {
        return _target2 == _last2._pos2;
    }
    int nbLegalActions() const override {
        return _legalActions.size();
    }
    const Action * getLegalAction(int iAction) const {
        assert(iAction >= 0);
        assert(iAction < nbLegalActions());
        return &(_legalActions[iAction]);
    }
    void applyAction(int iAction) override {
        if (iAction >= 0 and iAction < nbLegalActions())
            _last2 = {iAction};
    }
    std::string show() const override {
        std::ostringstream oss;
        oss << "Game2 " << _target2 << " ";
        oss << _last2.show() << std::endl;
        for (int i=0; i<nbLegalActions(); i++)
            oss << i << " ";
        oss << std::endl;
        return oss.str();
    }
};

///////////////////////////////////////////////////////////////////////////////
// Bots.
///////////////////////////////////////////////////////////////////////////////

struct BotHuman : Bot {
    int chooseAction(const Game & game) override {
        while (true) {
            try {
                std::string line;
                std::getline(std::cin, line);
                int n = stoi(line);
                if (n < game.nbLegalActions())
                    return n;
                else
                    std::cout << "bad input" << std::endl;
            }
            catch (...) {
                std::cout << "bad index" << std::endl;
            }
        }
    }
};

struct BotRandom : Bot {
    std::mt19937 _engine;
    BotRandom() : _engine(std::random_device{}()) {}
    int chooseAction(const Game & game) override {
        int nbActions = game.nbLegalActions();
        std::uniform_int_distribution<int> distribution(0, nbActions);
        return distribution(_engine);
    }
};

///////////////////////////////////////////////////////////////////////////////
// Factories.
///////////////////////////////////////////////////////////////////////////////

std::unique_ptr<Game> gameFactory(const std::string & gameName) {
    if (gameName == "Game1") return std::make_unique<Game1>();
    else if (gameName == "Game2") return std::make_unique<Game2>();
    else return {};
}

std::unique_ptr<Bot> botFactory(const std::string & botName) {
    if (botName == "BotHuman") return std::make_unique<BotHuman>();
    else if (botName == "BotRandom") return std::make_unique<BotRandom>();
    else return {};
}

///////////////////////////////////////////////////////////////////////////////
// Main program.
///////////////////////////////////////////////////////////////////////////////

void play(Game & game, Bot & bot) {
    while (not game.isTerminated()) {
        std::cout << game.show();
        int iAction = bot.chooseAction(game);
        std::cout << "-> " << iAction << std::endl;
        game.applyAction(iAction);
    }
}

int main(int argc, char ** argv) {
    if (argc != 3) {
        std::cout << "args: <game> <bot>" << std::endl;
        exit(-1);
    }
    const std::string gameName = argv[1];
    const std::string botName = argv[2];

    auto game = gameFactory(gameName);
    if (not game) {
        std::cout << "unknown game" << std::endl;
        exit(-1);
    }

    auto bot = botFactory(botName);
    if (not bot) {
        std::cout << "unknown bot" << std::endl;
        exit(-1);
    }

    play(*game, *bot);
    std::cout << game->show();

    return 0;
}

