with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "line-of-gaming";
  src = ./.;

  buildInputs = [
    cmake
    cpputest
    pkgconfig
    SDL2
    SDL2_gfx
    SDL2_ttf
  ];

  doCheck = true;

  checkPhase = ''
    LD_LIBRARY_PATH=$(pwd) ./main-test
  '';

}

