#include "Game.hpp"

#include <iostream>

bool Game::isPlaying() const {
    Status status = getStatus();
    return status == Status::Play0 or status == Status::Play1;
}

