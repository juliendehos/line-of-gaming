#pragma once

#include <memory>

enum Status {Play0, Play1, Win0, Win1, Tie};

class Game {
    public:
        virtual ~Game() = default;
        bool isPlaying() const;

        virtual void reset() = 0;
        virtual bool applyAction(int iAction) = 0;
        virtual int getNbLegalActions() const = 0;
        virtual Status getStatus() const = 0;
        virtual std::unique_ptr<Game> clone() const = 0;
};

