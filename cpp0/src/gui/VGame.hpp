#pragma once

#include "../core/Bot.hpp"
#include "../core/Game.hpp"

#include <memory>
#include <optional>
#include <SDL.h>

class VGame : public virtual Game {

    protected:
        const int _width;
        const int _height;

        SDL_Window * _window;
        SDL_Renderer * _renderer;

        SDL_Surface * _win0Surface;
        SDL_Texture * _win0Texture;
        SDL_Surface * _win1Surface;
        SDL_Texture * _win1Texture;
        SDL_Surface * _tieSurface;
        SDL_Texture * _tieTexture;

        std::optional<int> _selectedAction;

    public:
        VGame(int width, int height);
        virtual ~VGame();
        void run(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1);
        std::optional<int> getSelectedAction() const;

    private:
        bool step(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1);

    protected:
        virtual void display() = 0;
        virtual void click(int x, int y) = 0;

};

