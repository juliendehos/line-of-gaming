#pragma once

#include "../game/GameTictactoe.hpp"
#include "VGame.hpp"

class VGameTictactoe : public VGame, public GameTictactoe {

    public:
        VGameTictactoe(int width, int height);

    protected:
        void display() override;
        void click(int x, int y) override;

};

