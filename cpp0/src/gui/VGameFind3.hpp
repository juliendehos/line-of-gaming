#pragma once

#include "../game/GameFind3.hpp"
#include "VGame.hpp"

class VGameFind3 : public virtual VGame, public virtual GameFind3 {
    private:
        std::vector<SDL_Surface *> _msgSurfaces;
        std::vector<SDL_Texture *> _msgTextures;

    public:
        VGameFind3(int width, int height);
        ~VGameFind3();

    protected:
        void display() override;
        void click(int x, int y) override;

};

