#include "BotHuman.hpp"
#include "VGame.hpp"

#include <iostream>

std::optional<int> BotHuman::chooseAction(const Game & game) {
    auto uptrVGame = dynamic_cast<const VGame *>(&game);
    return uptrVGame->getSelectedAction();
}

