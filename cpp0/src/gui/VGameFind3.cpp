#include "VGameFind3.hpp"

#include <algorithm>
#include <iostream>
#include <SDL2/SDL_ttf.h>

VGameFind3::VGameFind3(int width, int height) :
    VGame(width, height),
    _msgSurfaces(20),
    _msgTextures(20)
{

    TTF_Font * msgFont = TTF_OpenFont("share/LiberationMonoBold.ttf", 64);
    SDL_Color msgColor0 = {0, 0, 0};
    SDL_Color msgColor127 = {127, 127, 127};

    for (int k=0; k<10; ++k) {
        auto txt = std::to_string(k).c_str();
        _msgSurfaces[k] = TTF_RenderText_Solid(msgFont, txt, msgColor0);
        _msgTextures[k] = SDL_CreateTextureFromSurface(_renderer, _msgSurfaces[k]);
        _msgSurfaces[10+k] = TTF_RenderText_Solid(msgFont, txt, msgColor127);
        _msgTextures[10+k] = SDL_CreateTextureFromSurface(_renderer, _msgSurfaces[10+k]);
    }

}

VGameFind3::~VGameFind3() {
    for (auto s : _msgSurfaces)
        SDL_FreeSurface(s);

    for (auto t : _msgTextures)
        SDL_DestroyTexture(t);
}

void VGameFind3::display() {

    const int width2 = _width / 2;
    const int height5 = _height / 5;

    // clear VGame
    SDL_SetRenderDrawColor (_renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(_renderer);

    // draw background
    SDL_Rect fillRect = { 0, 0, _width, _height };
    SDL_SetRenderDrawColor(_renderer, 0x44, 0xBB, 0xFF, 0xFF);
    SDL_RenderFillRect(_renderer, &fillRect);

    // draw lines
    SDL_SetRenderDrawColor(_renderer, 0x00, 0x00, 0x00, 0xFF);        
    SDL_RenderDrawLine(_renderer, width2, 0, width2, _height);
    for (int k=1; k<5; ++k) {
        const int i = k * height5;
        SDL_RenderDrawLine(_renderer, 0, i, _width, i);
    }

    // draw numbers
    SDL_Rect msgRect; 
    msgRect.w = 20;
    msgRect.h = 40;
    const int dx = (width2 - msgRect.w) / 2;
    const int dy = (height5 - msgRect.h) / 2;
    for (int i=0; i<5; ++i) {
        for (int j=0; j<2; ++j) {
            const int k = i*2 + j;
            msgRect.x = j*width2 + dx;
            msgRect.y = i*height5 + dy;
            auto it = std::find(_history.begin(), _history.end(), k);
            if (it == _history.end())
                SDL_RenderCopy(_renderer, _msgTextures[k], nullptr, &msgRect);
            else
                SDL_RenderCopy(_renderer, _msgTextures[10+k], nullptr, &msgRect);
        }
    }

}

void VGameFind3::click(int x, int y) {
    const int i = y * 5 / _height;
    const int j = x * 2 / _width;
    _selectedAction = i*2 + j;
}

