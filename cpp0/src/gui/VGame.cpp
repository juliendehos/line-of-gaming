#include "BotHuman.hpp"

#include "VGame.hpp"
#include "VGameFind3.hpp"
#include "VGameTictactoe.hpp"

#include <memory>
#include <iostream>
#include <SDL2/SDL_ttf.h>

VGame::VGame(int width, int height) :
    _width(width),
    _height(height),
    _window(nullptr),
    _renderer(nullptr)
{

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "init error: " << SDL_GetError() << std::endl;
        exit(-1);
    }

    _window = SDL_CreateWindow("line-of-gaming", 
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            _width, _height, SDL_WINDOW_SHOWN);
    if (_window == nullptr) {
        std::cerr << "window error: " << SDL_GetError() << std::endl;
        exit(-1);
    }

    _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
    if (_renderer == nullptr) {
        std::cerr << "renderer error: " << SDL_GetError() << std::endl;
        exit(-1);
    }

    if (TTF_Init() == -1) {
        std::cerr << "TTF_Init: " << TTF_GetError() << std::endl;
        exit(-1);
    }

    TTF_Font * msgFont = TTF_OpenFont("share/LiberationMonoBold.ttf", 64);
    SDL_Color msgColor0 = {255, 0, 0};
 
    _win0Surface = TTF_RenderText_Solid(msgFont, "win0", msgColor0);
    _win0Texture = SDL_CreateTextureFromSurface(_renderer, _win0Surface);
    _win1Surface = TTF_RenderText_Solid(msgFont, "win1", msgColor0);
    _win1Texture = SDL_CreateTextureFromSurface(_renderer, _win1Surface);
    _tieSurface = TTF_RenderText_Solid(msgFont, "tie", msgColor0);
    _tieTexture = SDL_CreateTextureFromSurface(_renderer, _tieSurface);

}

VGame::~VGame() {

    SDL_FreeSurface(_win0Surface);
    SDL_DestroyTexture(_win0Texture);
    SDL_FreeSurface(_win1Surface);
    SDL_DestroyTexture(_win1Texture);
    SDL_FreeSurface(_tieSurface);
    SDL_DestroyTexture(_tieTexture);

    TTF_Quit();
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
    SDL_Quit();

}

void VGame::run(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1) {
    while (step(bot0, bot1));
}

bool VGame::step(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1) {

    // handle event
    SDL_Event event;
    if (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            return false;
        }

        if (event.button.button == SDL_BUTTON_RIGHT 
                and event.type == SDL_MOUSEBUTTONUP) {
            return false;
        }

        if (event.button.button == SDL_BUTTON_MIDDLE 
                and event.type == SDL_MOUSEBUTTONUP) {
            reset();
            std::cout << "reset" << std::endl;
        }

        if (event.button.button == SDL_BUTTON_LEFT 
                and event.type == SDL_MOUSEBUTTONUP
                and isPlaying()) {

            // handle click
            click(event.button.x, event.button.y);

            // get bot and apply action
            do {
                Status status0 = getStatus();
                auto & bot = status0 == Status::Play0 ? bot0 : bot1;
                auto player = status0 == Status::Play0 ? "0" : "1";
                auto k = bot->chooseAction(*this);
                if (not k.has_value())
                    break;
                std::cout << player << " plays " << *k << std::endl;
                applyAction(*k);
                _selectedAction.reset();

            } while (isPlaying());

            Status status1 = getStatus();
            if (status1 == Status::Win0)
                std::cout << "win0" << std::endl;
            else if (status1 == Status::Win1)
                std::cout << "win1" << std::endl;
            else if (status1 == Status::Tie)
                std::cout << "tie" << std::endl;
        }

        display();

        if (not isPlaying()) {
            Status status = getStatus();
            SDL_Rect msgRect; 
            msgRect.w = 100;
            msgRect.h = 40;
            msgRect.x = 150;
            msgRect.y = 200;
            if (status == Status::Win0)
                SDL_RenderCopy(_renderer, _win0Texture, nullptr, &msgRect);
            else if (status == Status::Win1)
                SDL_RenderCopy(_renderer, _win1Texture, nullptr, &msgRect);
            else
                SDL_RenderCopy(_renderer, _tieTexture, nullptr, &msgRect);
        }

        // run rendering
        SDL_RenderPresent(_renderer);
        SDL_UpdateWindowSurface(_window);

    }

    return true;
}

std::optional<int> VGame::getSelectedAction() const {
    return _selectedAction;
}

