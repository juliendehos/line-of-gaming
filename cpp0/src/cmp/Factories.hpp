#include <functional>
#include <iostream>
#include <map>
#include <memory>

///////////////////////////////////////////////////////////////////////////////
// game
///////////////////////////////////////////////////////////////////////////////

#include "../core/Game.hpp"

using mkGame_t = std::function<std::unique_ptr<Game>()>;

extern std::map<std::string, mkGame_t> GameFactory;

void checkGame(const std::string & name);

void showGameFactory(std::ostream & os);

///////////////////////////////////////////////////////////////////////////////
// bot
///////////////////////////////////////////////////////////////////////////////

#include "../core/Bot.hpp"

using mkBot_t = std::function<std::unique_ptr<Bot>(const std::string &)>;

extern std::map<std::string, mkBot_t> BotFactory;

void checkBot(const std::string & name);

void showBotFactory(std::ostream & os);

