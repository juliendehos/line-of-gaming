#include "Factories.hpp"

int main(int argc, char ** argv) {

    // check usage
    if (argc != 7) {
        std::cout << "usage: " << argv[0] 
            << " <nruns> <game> <bot0> <pbot0> <bot1> <pbot1>\n";
        std::cout << "example: " 
            << argv[0] << " 100 tictactoe random '' mc 'nSims=10'\n";
        showGameFactory(std::cout);
        showBotFactory(std::cout);
        exit(-1);
    }

    // get cli arguments
    const int nruns = atoi(argv[1]);
    const std::string game = argv[2];
    const std::string bot0 = argv[3];
    const std::string pbot0 = argv[4];
    const std::string bot1 = argv[5];
    const std::string pbot1 = argv[6];

    // create game
    checkGame(game);
    auto uptrGame = GameFactory[game]();

    // create bots
    checkBot(bot0);
    auto uptrBot0 = BotFactory[bot0](pbot0);
    checkBot(bot1);
    auto uptrBot1 = BotFactory[bot1](pbot1);

    // compare bots
    int nbot0 = 0;
    int nbot1 = 0;
    int nties = 0;

    // prepare pointers for swapping first/second players
    Bot * ptrBotA = uptrBot0.get();
    Bot * ptrBotB = uptrBot1.get();
    int * ptrNbotA = &nbot0;
    int * ptrNbotB = &nbot1;

    for (int n=0; n<nruns; n++) {
        // run one game
        uptrGame->reset();
        while (uptrGame->isPlaying()) {
            Status status = uptrGame->getStatus();
            Bot * ptrBot = status == Status::Play0 ? ptrBotA : ptrBotB;
            auto k = ptrBot->chooseAction(*uptrGame);
            if (not k)
                std::cerr << "no action" << std::endl;
            uptrGame->applyAction(*k);
        }

        // store result
        Status status1 = uptrGame->getStatus();
        if (status1 == Status::Tie)
            nties++;
        else {
            if (status1 == Status::Win0)
                (*ptrNbotA)++;
            else
                (*ptrNbotB)++;
        }

        // swap players for next run
        std::swap(ptrBotA, ptrBotB);
        std::swap(ptrNbotA, ptrNbotB);

        std::cerr << ".";
    }

    // display results
    std::cout << "\nnruns;game;bot0;" 
        << uptrBot0->csvKeys("bot0_")
        << ";bot1;"
        << uptrBot1->csvKeys("bot1_")
        << ";nbot0;nbot1;nties\n";
    std::cout
        << nruns << ";" 
        << game << ";" 
        << bot0 << ";"
        << uptrBot0->csvValues() << ";"
        << bot1 << ";"
        << uptrBot1->csvValues() << ";"
        << nbot0 << ";"
        << nbot1 << ";"
        << nties << std::endl;

    return 0;
}

