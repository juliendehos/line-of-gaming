#include "GameTictactoe.hpp"

#include <cassert>

GameTictactoe::GameTictactoe() {
    reset();
}

void GameTictactoe::reset() {
    std::fill(_board.begin(), _board.end(), std::array<int,3>{-1, -1, -1});
    _history.clear();
    _status = Status::Play0;
    findActions();
}

int GameTictactoe::getInBoard(int i, int j) const {
    assert(i >= 0 and i < 3);
    assert(j >= 0 and j < 3);
    return _board[i][j];
}

const std::vector<GameTictactoe::Action> & GameTictactoe::getLegalActions() const {
    return _legalActions;
}

bool GameTictactoe::applyAction(int i, int j) {
    if (i < 0 or i >= 3 or j < 0 or j >= 3 or _board[i][j] != -1)
        return false;
    update(i, j);
    return true;
}

bool GameTictactoe::applyAction(int iAction) {
    if (iAction < 0 or iAction >= int(_legalActions.size()))
        return false;
    auto ij = _legalActions[iAction]; 
    assert(ij.first >= 0 or ij.first < 3);
    assert(ij.second >= 0 or ij.second < 3);
    assert(_board[ij.first][ij.second] == -1);
    assert(_status == Status::Play0 or _status == Status::Play1);
    update(ij.first, ij.second);
    return true;
}

int GameTictactoe::getNbLegalActions() const {
    return _legalActions.size();
}

Status GameTictactoe::getStatus() const {
    return _status;
}

void GameTictactoe::findActions() {
    _legalActions.clear();
    for (unsigned i=0; i<3; ++i) {
        for (unsigned j=0; j<3; ++j) {
            if (_board[i][j] == -1)
                _legalActions.push_back({i, j});
        }
    }
}

void GameTictactoe::update(int i, int j) {

    _board[i][j] = _status;
    _history.push_back({i,j});
    findActions();

    const auto & b = _board;
    // find a win
    if ((b[0][0] == _status and b[1][1] == _status and b[2][2] == _status) or
        (b[0][2] == _status and b[1][1] == _status and b[2][0] == _status) or
        (b[i][0] == _status and b[i][1] == _status and b[i][2] == _status) or
        (b[0][j] == _status and b[1][j] == _status and b[2][j] == _status)) {
        _status = _status == Status::Play0 ? Status::Win0 : Status::Win1;
    }
    // find a tie
    else if (_legalActions.size() == 0) {
        _status = Status::Tie;
    }
    // change current player
    else {
        _status = _status == Play0 ? Play1 : Play0;
    }
}

std::unique_ptr<Game> GameTictactoe::clone() const {
    return std::make_unique<GameTictactoe>(*this);
}

