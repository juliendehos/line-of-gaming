#include "GameFind3.hpp"

GameFind3::GameFind3() {
    reset();
}

void GameFind3::reset() {
    _history.clear();
}

const std::vector<int> GameFind3::getHistory() {
    return _history;
}

bool GameFind3::applyAction(int iAction) {
    const Status status = getStatus();
    if ((status == Status::Play0 or status == Status::Play1)
            and iAction >= 0 and iAction < 10) {
        _history.push_back(iAction);
        return true;
    }
    else
        return false;
}

int GameFind3::getNbLegalActions() const {
    return 10;
}

Status GameFind3::getStatus() const {
    const int size = _history.size();
    if (size > 0 and _history.back() == 3)
        return size % 2 == 1 ? Status::Win0 : Status::Win1;
    else if (size < 4) 
        return size % 2 == 0 ? Status::Play0 : Status::Play1;
    else
        return Status::Tie;
}

std::unique_ptr<Game> GameFind3::clone() const {
    return std::make_unique<GameFind3>(*this);
}

