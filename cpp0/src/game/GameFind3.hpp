#pragma once

#include "../core/Game.hpp"

#include <vector>

class GameFind3 : public virtual Game {
    protected:
        std::vector<int> _history;

    public:
        GameFind3();
        const std::vector<int> getHistory();

        void reset() override;
        bool applyAction(int iAction) override;
        int getNbLegalActions() const override;
        Status getStatus() const override;
        std::unique_ptr<Game> clone() const override;
};

