#pragma once

#include "../core/Bot.hpp"
#include "../core/Game.hpp"

class VGame : public virtual Game {

    public:
        virtual ~VGame() = default;
        void run(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1);

        virtual void display() = 0;
        virtual int findLegalAction(const std::string & input) const = 0;
};

