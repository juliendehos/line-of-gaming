#include "Factories.hpp"

#include <iostream>

int main(int argc, char ** argv) {

    // check usage
    if (argc != 6) {
        std::cout << "usage: " << argv[0]
            << " <game> <bot0> <pbot0> <bot1> <pbot1>\n";
        std::cout << "example: " << argv[0] 
            << " tictactoe random '' mc 'nSims=10'\n";
        showVGameFactory(std::cout);
        showBotFactory(std::cout);
        exit(-1);
    }

    // get cli arguments
    const std::string game = argv[1];
    const std::string bot0 = argv[2];
    const std::string pbot0 = argv[3];
    const std::string bot1 = argv[4];
    const std::string pbot1 = argv[5];

    // create game
    checkVGame(game);
    auto uptrVGame = VGameFactory[game]();

    // create bots
    checkBot(bot0);
    auto uptrBot0 = BotFactory[bot0](pbot0);
    checkBot(bot1);
    auto uptrBot1 = BotFactory[bot1](pbot1);

    // main loop
    uptrVGame->run(uptrBot0, uptrBot1);

    // display final game
    std::cout << std::endl;
    uptrVGame->display();

    // display result
    std::cout << std::endl;
    std::cout << "result: ";
    Status status1 = uptrVGame->getStatus();
    if (status1 == Status::Win0)
        std::cout << "win0";
    else if (status1 == Status::Win1)
        std::cout << "win1";
    else
        std::cout << "tie";
    std::cout << std::endl;

    return 0;
}

