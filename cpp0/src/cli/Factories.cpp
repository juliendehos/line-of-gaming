#include "Factories.hpp"

#include <iostream>

///////////////////////////////////////////////////////////////////////////////
// VGame
///////////////////////////////////////////////////////////////////////////////

#include "VGameFind3.hpp"
#include "VGameTictactoe.hpp"

std::map<std::string, mkVGame_t> VGameFactory {

    {"find3", [] () {
        return std::make_unique<VGameFind3>(); }},

    {"tictactoe", [] () {
        return std::make_unique<VGameTictactoe>(); }},

};

void checkVGame(const std::string & name) {
    if (VGameFactory.find(name) == VGameFactory.end()) {
        std::cerr << "error: this game is unknown (" << name << ")\n";
        showVGameFactory(std::cerr);
        exit(-1);
    }
}

void showVGameFactory(std::ostream & os) {
    os << "games:\n";
    for (const auto & g : VGameFactory)
        os << "  - " << g.first << "\n";
}

///////////////////////////////////////////////////////////////////////////////
// bot
///////////////////////////////////////////////////////////////////////////////

#include "BotHuman.hpp"
#include "../bot/BotRandom.hpp"
#include "../bot/BotMc.hpp"

std::map<std::string, mkBot_t> BotFactory {

    {"human", [] (const std::string &) {
        return std::make_unique<BotHuman>(); }},

    {"random", [] (const std::string &) {
        return std::make_unique<BotRandom>(); }},

    {"mc", [] (const std::string & input) {
        return std::make_unique<BotMc>(input); }}

};

void checkBot(const std::string & name) {
    if (BotFactory.find(name) == BotFactory.end()) {
        std::cerr << "error: this bot is unknown (" << name << ")\n";
        showBotFactory(std::cerr);
        exit(-1);
    }
}

void showBotFactory(std::ostream & os) {
    os << "bots:\n";
    for (const auto & b : BotFactory)
        os << "  - " << b.first << " (" << b.second("")->show() << ")\n";
}

