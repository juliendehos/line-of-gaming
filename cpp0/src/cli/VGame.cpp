#include "VGame.hpp"

#include <iostream>

void VGame::run(std::unique_ptr<Bot> & bot0, std::unique_ptr<Bot> & bot1) {

    while (isPlaying()) {

        // display game
        std::cout << std::endl;
        display();

        // get bot
        Status status = getStatus();
        auto & bot = status == Status::Play0 ? bot0 : bot1;
        auto player = status == Status::Play0 ? "0" : "1";
        std::cout << "player: " << player << std::endl;

        // get and apply action
        auto k = bot->chooseAction(*this);
        // TODO check k ?
        std::cout << "playing: " << *k << " -> ";
        if (applyAction(*k))
            std::cout << "ok" << std::endl;
        else
            std::cout << "ko" << std::endl;
    }

}

