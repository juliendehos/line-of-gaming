#pragma once

#include "../game/GameTictactoe.hpp"
#include "VGame.hpp"

class VGameTictactoe : public VGame, public GameTictactoe {
    public:
        void display() override;
        int findLegalAction(const std::string & input) const override;
    protected:
        static std::string actionToString(const Action & action);
        static Action stringToAction(const std::string & string);
};

