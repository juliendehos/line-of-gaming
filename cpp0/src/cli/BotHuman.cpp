#include "BotHuman.hpp"
#include "VGame.hpp"

#include <iostream>
#include <stdexcept>

std::optional<int> BotHuman::chooseAction(const Game & game) {
    auto ptrVGame = dynamic_cast<const VGame *>(&game);
    while (true) {
        try {
            std::cout << "action? ";
            std::cout.flush();
            std::string line;
            std::getline(std::cin, line);
            return ptrVGame->findLegalAction(line);
        }
        catch (std::invalid_argument e) {
            std::cout << e.what() << std::endl;;
        }
        catch (...) {
            std::cout << "invalid input" << std::endl;;
        }
    }
    return {};
}

