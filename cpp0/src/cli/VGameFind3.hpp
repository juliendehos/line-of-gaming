#pragma once

#include "../game/GameFind3.hpp"
#include "VGame.hpp"

class VGameFind3 : public VGame, public GameFind3 {
    public:
        void display() override;
        int findLegalAction(const std::string & input) const override;
};

