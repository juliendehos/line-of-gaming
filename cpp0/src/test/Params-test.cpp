#include "../core/Params.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupParams) { };

TEST(GroupParams, init1) {
    Params params;
    CHECK_EQUAL(0, params.size());
}

TEST(GroupParams, init2) {
    Params params { {"p1", 1}, {"p2", 2} };
    CHECK_EQUAL(2, params.size());
    CHECK_EQUAL(1, params["p1"]);
    CHECK_EQUAL(2, params["p2"]);
}

TEST(GroupParams, show0) {
    Params params;
    CHECK_EQUAL("", params.show());
}

TEST(GroupParams, show1) {
    Params params { {"p1", 1} };
    CHECK_EQUAL("p1=1", params.show());
}

TEST(GroupParams, show2) {
    Params params { {"p1", 1}, {"p2", 2} };
    CHECK_EQUAL("p1=1,p2=2", params.show());
}

TEST(GroupParams, csvKeys0) {
    Params params;
    CHECK_EQUAL("", params.csvKeys("b1_"));
}

TEST(GroupParams, csvKeys1) {
    Params params { {"p1", 1} };
    CHECK_EQUAL("b1_p1", params.csvKeys("b1_"));
}

TEST(GroupParams, csvKeys2) {
    Params params { {"p1", 1}, {"p2", 2} };
    CHECK_EQUAL("b1_p1;b1_p2", params.csvKeys("b1_"));
}

TEST(GroupParams, csvValues0) {
    Params params;
    CHECK_EQUAL("", params.csvValues());
}

TEST(GroupParams, csvValues1) {
    Params params { {"p1", 1} };
    CHECK_EQUAL("1", params.csvValues());
}

TEST(GroupParams, csvValues2) {
    Params params { {"p1", 1}, {"p2", 2} };
    CHECK_EQUAL("1;2", params.csvValues());
}

TEST(GroupParams, parse1) {
    Params params { {"p1", 0}, {"p2", 0} };
    params.parse("p2=2,p1=1");
    CHECK_EQUAL(2, params.size());
    CHECK_EQUAL(1, params["p1"]);
    CHECK_EQUAL(2, params["p2"]);
}

TEST(GroupParams, parse2) {
    Params params { {"nSims", 1}, {"kTie", 1} };
    params.parse("nSims=1000,kTie=0.5");
    CHECK_EQUAL(2, params.size());
    CHECK_EQUAL(1000, params["nSims"]);
    CHECK_EQUAL(0.5, params["kTie"]);
}

TEST(GroupParams, parse3) {
    Params params { {"nSims", 1}, {"kTie", 1} };
    params.parse("nSims=1000,kTie=0.5");
    CHECK_EQUAL(2, params.size());
    CHECK_EQUAL(1000, params["nSims"]);
    CHECK_EQUAL(0.5, params["kTie"]);
}

TEST(GroupParams, parse4) {
    try {
        Params params { {"nSims", 1}, {"kTie", 1} };
        params.parse("nSims=1000,pp=0.5");
        FAIL("exception not raised");
    }
    catch (const std::invalid_argument & e) {
        CHECK_EQUAL(std::string("unknown key: pp"), e.what());
    }
    catch (...) {
        FAIL("unknown exception");
    }
}

TEST(GroupParams, parse5) {
    try {
        Params params { {"nSims", 1}, {"kTie", 1} };
        params.parse("42,0.5");
        FAIL("exception not raised");
    }
    catch (const std::invalid_argument & e) {
        CHECK_EQUAL(std::string("failed to parse value in: 42"), e.what());
    }
    catch (...) {
        FAIL("unknown exception");
    }
}

