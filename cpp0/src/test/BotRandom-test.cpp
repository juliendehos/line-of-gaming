
#include "../bot/BotRandom.hpp"
#include "../game/GameFind3.hpp"

#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>

TEST_GROUP(GroupBotRandom) { };

TEST(GroupBotRandom, avg1) {
    BotRandom b;
    GameFind3 g;
    const int N = 10000;
    const int s0 = 44000;
    const int s1 = 46000;
    int s = 0;
    for (int n=0; n<N; ++n) {
        auto k = b.chooseAction(g);
        CHECK(k.has_value());
        CHECK(*k >= 0);
        CHECK(*k < 10);
        s += *k;
    }
    CHECK(s > s0);
    CHECK(s < s1);
}

TEST(GroupBotRandom, hist1) {
    BotRandom b;
    GameFind3 g;
    const int N = 10000;
    const int h0 = 900;
    const int h1 = 1100;
    std::vector<int> hist(g.getNbLegalActions(), 0);
    for (int n=0; n<N; ++n) {
        auto k = b.chooseAction(g);
        CHECK(k.has_value());
        CHECK(*k >= 0);
        CHECK(*k < 10);
        hist[*k]++;
    }
    int s = 0;
    for (int h : hist) {
        s += h;
        CHECK(h > h0);
        CHECK(h < h1);
    }
    CHECK(s == N);
}

