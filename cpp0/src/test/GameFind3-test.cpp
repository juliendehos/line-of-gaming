
#include "../game/GameFind3.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupGameFind3) { };

TEST(GroupGameFind3, init1) {
    GameFind3 g;
    CHECK_EQUAL(0, g.getHistory().size());
    CHECK_EQUAL(Status::Play0, g.getStatus());
    CHECK_EQUAL(10, g.getNbLegalActions());
}

TEST(GroupGameFind3, applyAction5) {
    GameFind3 g;
    CHECK_EQUAL(Status::Play0, g.getStatus());
    g.applyAction(5);
    CHECK_EQUAL(Status::Play1, g.getStatus());

    CHECK_EQUAL(1, g.getHistory().size());
    CHECK_EQUAL(5, g.getHistory()[0]);
}

TEST(GroupGameFind3, applyAction23) {
    GameFind3 g;
    CHECK_EQUAL(Status::Play0, g.getStatus());
    g.applyAction(2);
    CHECK_EQUAL(Status::Play1, g.getStatus());
    g.applyAction(3);
    CHECK_EQUAL(Status::Win1, g.getStatus());

    CHECK_EQUAL(2, g.getHistory().size());
    CHECK_EQUAL(2, g.getHistory()[0]);
    CHECK_EQUAL(3, g.getHistory()[1]);
}

TEST(GroupGameFind3, applyAction9876) {
    GameFind3 g;
    CHECK_EQUAL(Status::Play0, g.getStatus());
    g.applyAction(9);
    CHECK_EQUAL(Status::Play1, g.getStatus());
    g.applyAction(8);
    CHECK_EQUAL(Status::Play0, g.getStatus());
    g.applyAction(7);
    CHECK_EQUAL(Status::Play1, g.getStatus());
    g.applyAction(6);
    CHECK_EQUAL(Status::Tie, g.getStatus());

    CHECK_EQUAL(4, g.getHistory().size());
    CHECK_EQUAL(9, g.getHistory()[0]);
    CHECK_EQUAL(8, g.getHistory()[1]);
    CHECK_EQUAL(7, g.getHistory()[2]);
    CHECK_EQUAL(6, g.getHistory()[3]);
}

