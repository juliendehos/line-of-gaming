#include "GameGomoku.hpp"

#include <cassert>

void GameGomoku::init() {
    _nbPlayers = 2;  // TODO
    reset();
}

int & GameGomoku::board(int i, int j) {
    return _board[i*_nbI+j];
}

const int & GameGomoku::board(int i, int j) const {
    return _board[i*_nbI+j];
}

int GameGomoku::getCell(int i, int j) const {
    return board(i, j);
}

void GameGomoku::reset() {
    Game::reset();
    _nbI = 7;
    _nbJ = 7;
    // _nbI = 15;
    // _nbJ = 15;
    _board.resize(_nbI*_nbJ);
    std::fill(_board.begin(), _board.end(), -1);
    _legalActions.reserve(_nbI*_nbJ);
    _history.clear();
    findActions();
}

bool GameGomoku::applyAction(int iAction) {

    assert(iAction >= 0);
    assert(iAction < getNbLegalActions());
    Action action = _legalActions[iAction];

    if (board(action.first, action.second) != -1)
        return false;

    update(action.first, action.second);
    return true;
}

void GameGomoku::update(int i, int j) {
    board(i, j) = _currentPlayer;
    _history.push_back({i,j});
    findActions();

    // find a win
    if (computeLength(i, j, 1, 0, _currentPlayer) >= 5
            or computeLength(i, j, 0, 1, _currentPlayer) >= 5
            or computeLength(i, j, 1, 1, _currentPlayer) >= 5
            or computeLength(i, j, 1, -1, _currentPlayer) >= 5)
    {
        _rewards[_currentPlayer] = 1;
        _winners = {_currentPlayer};
        _isPlaying = false;
    }
    // find a tie
    else if (_legalActions.size() == 0) {
        _winners = {};
        _isPlaying = false;
    }
    // change current player
    else {
        _currentPlayer = (_currentPlayer + 1) % _nbPlayers;
    }
}

int GameGomoku::computeLength(int i, int j, int di, int dj, int player) const {

    int k = -1;

    int i0 = i;
    int j0 = j;
    while (i0>=0 and i0<_nbI and j0>=0 and j0<_nbJ and board(i0, j0) == player) {
        i0 -= di;
        j0 -= dj;
        k++;
    }

    int i1 = i;
    int j1 = j;
    while (i1>=0 and i1<_nbI and j1>=0 and j1<_nbJ and board(i1, j1) == player) {
        i1 += di;
        j1 += dj;
        k++;
    }

    return k;
}

void GameGomoku::findActions() {
    _legalActions.clear();
    for (int i=0; i<_nbI; i++)
        for (int j=0; j<_nbJ; j++)
            if (board(i, j) == -1)
                _legalActions.push_back({i,j});
}

int GameGomoku::getNbLegalActions() const {
    return _legalActions.size();
}

std::unique_ptr<Game> GameGomoku::clone() const {
    return std::make_unique<GameGomoku>(*this);
}

