#include "GameFind3.hpp"

void GameFind3::init() {
    _nbPlayers = 2;  // TODO
    reset();
}

void GameFind3::reset() {
    Game::reset();
    _history.clear();
}

const std::vector<int> GameFind3::getHistory() {
    return _history;
}

bool GameFind3::applyAction(int iAction) {
    if (not _isPlaying or iAction < 0 or iAction >= 10) 
        return false;

    _history.push_back(iAction);
    const int size = _history.size();
    if (size > 0 and _history.back() == 3) {
        // win
        _rewards[_currentPlayer] = 1;
        _winners = {_currentPlayer};
        _isPlaying = false;
    }
    else if (size < 4) {
        // playing
        _currentPlayer = (_currentPlayer + 1) % 2;
    }
    else {
        // tie
        _winners = {};
        _isPlaying = false;
    }
    return true;
}

int GameFind3::getNbLegalActions() const {
    return 10;
}

std::unique_ptr<Game> GameFind3::clone() const {
    return std::make_unique<GameFind3>(*this);
}

