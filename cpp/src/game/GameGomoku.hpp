#pragma once

#include "../core/Game.hpp"

#include <array>
#include <vector>

class GameGomoku : public virtual Game {
    protected:
        using Action = std::pair<int,int>;
        int _nbI;
        int _nbJ;
        std::vector<int> _board;
        std::vector<Action> _history;
        std::vector<Action> _legalActions;

    public:
        void init();

        void reset() override;
        bool applyAction(int iAction) override;
        int getNbLegalActions() const override;
        std::unique_ptr<Game> clone() const override;

        int getCell(int i, int j) const;

    private:
        const int & board(int i, int j) const;
        int & board(int i, int j);
        void findActions();
        void update(int i, int j);
        int computeLength(int i, int j, int di, int dj, int player) const;
};

