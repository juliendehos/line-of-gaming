#pragma once

#include "../core/Game.hpp"

#include <array>
#include <vector>

class GameTictactoe : public virtual Game {
    protected:
        using Action = std::pair<int,int>;
        std::array<std::array<int,3>,3> _board;
        std::vector<Action> _history;
        std::vector<Action> _legalActions;

    public:
        void init();
        int getInBoard(int i, int j) const;
        const std::vector<Action> & getLegalActions() const;
        bool applyAction(int i, int j);

        void reset() override;
        bool applyAction(int iAction) override;
        int getNbLegalActions() const override;
        std::unique_ptr<Game> clone() const override;

    private:
        void findActions();
        void update(int i, int j);
};

