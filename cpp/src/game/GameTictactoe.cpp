#include "GameTictactoe.hpp"

#include <cassert>

void GameTictactoe::init() {
    _nbPlayers = 2;  // TODO
    reset();
}

void GameTictactoe::reset() {
    Game::reset();
    std::fill(_board.begin(), _board.end(), std::array<int,3>{-1, -1, -1});
    _history.clear();
    findActions();
}

int GameTictactoe::getInBoard(int i, int j) const {
    assert(i >= 0 and i < 3);
    assert(j >= 0 and j < 3);
    return _board[i][j];
}

const std::vector<GameTictactoe::Action> & GameTictactoe::getLegalActions() const {
    return _legalActions;
}

bool GameTictactoe::applyAction(int i, int j) {
    if (i < 0 or i >= 3 or j < 0 or j >= 3 or _board[i][j] != -1)
        return false;
    update(i, j);
    return true;
}

bool GameTictactoe::applyAction(int iAction) {
    if (iAction < 0 or iAction >= int(_legalActions.size()))
        return false;
    auto ij = _legalActions[iAction]; 
    assert(ij.first >= 0 or ij.first < 3);
    assert(ij.second >= 0 or ij.second < 3);
    assert(_board[ij.first][ij.second] == -1);
    assert(_isPlaying);
    update(ij.first, ij.second);
    return true;
}

int GameTictactoe::getNbLegalActions() const {
    return _legalActions.size();
}

void GameTictactoe::findActions() {
    _legalActions.clear();
    for (unsigned i=0; i<3; ++i) {
        for (unsigned j=0; j<3; ++j) {
            if (_board[i][j] == -1)
                _legalActions.push_back({i, j});
        }
    }
}

void GameTictactoe::update(int i, int j) {

    _board[i][j] = _currentPlayer;
    _history.push_back({i,j});
    findActions();

    const auto & b = _board;
    // find a win
    if ((b[0][0] == _currentPlayer and b[1][1] == _currentPlayer and b[2][2] == _currentPlayer) or
        (b[0][2] == _currentPlayer and b[1][1] == _currentPlayer and b[2][0] == _currentPlayer) or
        (b[i][0] == _currentPlayer and b[i][1] == _currentPlayer and b[i][2] == _currentPlayer) or
        (b[0][j] == _currentPlayer and b[1][j] == _currentPlayer and b[2][j] == _currentPlayer)) {
        _rewards[_currentPlayer] = 1;
        _winners = {_currentPlayer};
        _isPlaying = false;
    }
    // find a tie
    else if (_legalActions.size() == 0) {
        _winners = {};
        _isPlaying = false;
    }
    // change current player
    else {
        _currentPlayer = (_currentPlayer + 1) % 2;
    }
}

std::unique_ptr<Game> GameTictactoe::clone() const {
    return std::make_unique<GameTictactoe>(*this);
}

