#include "Factories.hpp"
#include "../core/Params.hpp"

///////////////////////////////////////////////////////////////////////////////
// game
///////////////////////////////////////////////////////////////////////////////

#include "../game/GameFind3.hpp"
#include "../game/GameGomoku.hpp"
#include "../game/GameTictactoe.hpp"

std::map<std::string, mkGame_t> GameFactory {

    {"find3", [] () {
        return std::make_unique<GameFind3>(); }},

    {"gomoku", [] () {
        return std::make_unique<GameGomoku>(); }},

    {"tictactoe", [] () {
        return std::make_unique<GameTictactoe>(); }},

};

void checkGame(const std::string & name) {
    if (GameFactory.find(name) == GameFactory.end()) {
        std::cerr << "error: this game is unknown (" << name << ")\n";
        showGameFactory(std::cerr);
        exit(-1);
    }
}

void showGameFactory(std::ostream & os) {
    os << "games:\n";
    for (const auto & g : GameFactory)
        os << "  - " << g.first << "\n";
}

///////////////////////////////////////////////////////////////////////////////
// bot
///////////////////////////////////////////////////////////////////////////////

#include "../bot/BotRandom.hpp"
#include "../bot/BotMc.hpp"

std::map<std::string, mkBot_t> BotFactory {

    {"random", [] (const std::string &) {
        return std::make_unique<BotRandom>(); }},

    {"mc", [] (const std::string & input) {
        return std::make_unique<BotMc>(input); }}

};

void checkBot(const std::string & name) {
    if (BotFactory.find(name) == BotFactory.end()) {
        std::cerr << "error: this bot is unknown (" << name << ")\n";
        showBotFactory(std::cerr);
        exit(-1);
    }
}

void showBotFactory(std::ostream & os) {
    os << "bots:\n";
    for (const auto & b : BotFactory)
        os << "  - " << b.first << " (" << b.second("")->show() << ")\n";
}

