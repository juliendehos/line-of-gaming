#include "Params.hpp"

#include <sstream>

Params::Params() {
}

Params::Params(const std::initializer_list<param_pair_t> & init) :
    param_map_t(init)
{
}

void Params::parse(const std::string & input) {
    std::vector<std::string> params;
    std::istringstream iss(input);
    std::string token;
    while (std::getline(iss, token, ',')) {
        std::istringstream isstok(token);
        std::string key;
        if (std::getline(isstok, key, '=')) {
            std::string value;
            if (std::getline(isstok, value)) {
                auto it = find(key);
                if (it == end())
                    throw std::invalid_argument("unknown key: " + key);
                it->second = std::stod(value);
            }
            else {
                throw std::invalid_argument("failed to parse value in: " + token);
            }
        }
        else {
            throw std::invalid_argument("failed to parse key in: " + token);
        }
    }
}

std::string Params::show() const {
    if (empty())
        return {};
    std::ostringstream oss;
    auto it = begin();
    oss << it->first << '=' << it->second;
    for (++it; it != end(); ++it) {
        oss << ',' << it->first << '=' << it->second;
    }
    return oss.str();
}

std::string Params::csvKeys(const std::string & prefix) const {
    if (empty())
        return {};
    std::ostringstream oss;
    auto it = begin();
    oss << prefix << it->first;
    for (++it; it != end(); ++it) {
        oss << ';' << prefix << it->first;
    }
    return oss.str();
}

std::string Params::csvValues() const {
    if (empty())
        return {};
    std::ostringstream oss;
    auto it = begin();
    oss << it->second;
    for (++it; it != end(); ++it) {
        oss << ';' << it->second;
    }
    return oss.str();
}


/*
// TODO
std::vector<std::string> splitParams(const std::string & input, char delim) {
    std::vector<std::string> params;
    std::istringstream iss(input);
    std::string token;
    while (std::getline(iss, token, delim))
        params.push_back(token);
    return params;
}
*/

