#pragma once

#include <memory>
#include <vector>

class Game {
    protected:
        int _nbPlayers;
        std::vector<double> _rewards;
        std::vector<int> _winners;
        int _currentPlayer;
        bool _isPlaying;

    public:
        virtual ~Game() = default;

        virtual void init();
        virtual void reset();
        virtual std::unique_ptr<Game> clone() const = 0;
        virtual bool applyAction(int iAction) = 0;
        virtual int getNbLegalActions() const = 0;

        const std::vector<double> & getRewards() const;
        const std::vector<int> & getWinners() const;
        unsigned getNbPlayer() const;
        int getCurrentPlayer() const;
        bool isPlaying() const;
        bool isWinner(int player) const;
        bool hasTie() const;
};

