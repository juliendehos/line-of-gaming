#include "Game.hpp"

#include <algorithm>
#include <iostream>

void Game::init() {
    _nbPlayers = 0;  // TODO
}

void Game::reset() {
    _rewards.resize(_nbPlayers);
    std::fill(_rewards.begin(), _rewards.end(), 0);
    _winners.clear();
    _currentPlayer = 0;
    _isPlaying = true;
}

bool Game::isPlaying() const {
    return _isPlaying;
}

unsigned Game::getNbPlayer() const {
    return _rewards.size();
}

int Game::getCurrentPlayer() const {
    return _currentPlayer;
}

const std::vector<double> & Game::getRewards() const {
    return _rewards;
}

const std::vector<int> & Game::getWinners() const {
    return _winners;
}


bool Game::isWinner(int player) const {
    return std::find(_winners.begin(), _winners.end(), player) != _winners.end();
}

bool Game::hasTie() const {
    return _winners.size() != 1;
}

