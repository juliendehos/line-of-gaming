#pragma once

#include "Game.hpp"
#include "Params.hpp"

#include <memory>
#include <optional>

class Bot : public Params {
    public:
        Bot() = default;
        Bot(const std::initializer_list<param_pair_t> & paramsInit);
        virtual ~Bot() = default;
        virtual std::optional<int> chooseAction(const Game & game) = 0;
};

