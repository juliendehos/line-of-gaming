#pragma once

#include <map>
#include <string>
#include <vector>

using param_pair_t = std::pair<const std::string,double>;

using param_map_t = std::map<const std::string,double>;

class Params : public param_map_t {
    public:

        Params();
        Params(const std::initializer_list<param_pair_t> & init);

        // Parses input strings like 'nSims=1000,kTie=0.5'. Throws
        // std::invalid_argument if parsing fails or if unknown key.
        void parse(const std::string & input);

        // Shows params, e.g. 'nSims=1000,kTie=0.5'.
        std::string show() const;

        // Shows key, e.g. 'bot1_nSims;bot1_kTie'.
        std::string csvKeys(const std::string & prefix) const;

        // Shows values, e.g. '1000;0.5'.
        std::string csvValues() const;
};

/*
// TODO
std::vector<std::string> splitParams(const std::string & input, char delim);
*/

