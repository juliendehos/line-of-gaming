#include "Factories.hpp"

int main(int argc, char ** argv) {

    // check usage
    if (argc != 6) {
        std::cout << "usage: " << argv[0]
            << " <game> <bot0> <pbot0> <bot1> <pbot1>\n";
        std::cout << "example: " << argv[0] 
            << " tictactoe human '' mc 'nSims=100'\n";
        std::cout << "games:\n";
        showVGameFactory(std::cout);
        showBotFactory(std::cout);
        exit(-1);
    }

    // get cli arguments
    const std::string game = argv[1];
    const std::string bot0 = argv[2];
    const std::string pbot0 = argv[3];
    const std::string bot1 = argv[4];
    const std::string pbot1 = argv[5];

    // create game
    checkVGame(game);
    auto uptrVGame = VGameFactory[game](800, 800);
    uptrVGame->init();

    // create bots
    checkBot(bot0);
    auto uptrBot0 = BotFactory[bot0](pbot0);
    checkBot(bot1);
    auto uptrBot1 = BotFactory[bot1](pbot1);

    // main loop
    uptrVGame->run(uptrBot0, uptrBot1);

    return 0;
}

