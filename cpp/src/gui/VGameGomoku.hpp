#pragma once

#include "../game/GameGomoku.hpp"
#include "VGame.hpp"

class VGameGomoku : public VGame, public GameGomoku {

    public:
        VGameGomoku(int width, int height);

    protected:
        void display() override;
        void click(int x, int y) override;

};

