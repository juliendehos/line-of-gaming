#include "VGameGomoku.hpp"

#include <algorithm>
#include <iostream>
#include <SDL2/SDL2_gfxPrimitives.h>

VGameGomoku::VGameGomoku(int width, int height) :
    VGame(width, height)
{
}

void VGameGomoku::display() {
    const int height3 = _height / _nbI;
    const int width3 = _width / _nbJ;

    // clear VGame
    SDL_SetRenderDrawColor (_renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(_renderer);

    // draw background
    SDL_Rect fillRect = { 0, 0, _width, _height };
    SDL_SetRenderDrawColor(_renderer, 0x00, 0x60, 0x00, 0xFF);        
    SDL_RenderFillRect(_renderer, &fillRect);

    // draw lines
    SDL_SetRenderDrawColor(_renderer, 0x00, 0x00, 0x00, 0xFF);        
    for (int k=1; k<_nbI; ++k) {
        const int i = k * height3;
        const int j = k * width3;
        SDL_RenderDrawLine(_renderer, j, 0, j, _height);
        SDL_RenderDrawLine(_renderer, 0, i, _width, i);
    }

    // draw cells
    for (int i=0; i<_nbI; i++) {
        for (int j=0; j<_nbJ; j++) {
            int cell = getCell(i, j);
            const int radius = std::min(height3, width3) / 2 - 5;
            const int x = j*width3 + width3/2;
            const int y = i*height3 + height3/2;
            if (cell == 0)
                filledCircleRGBA(_renderer, x, y, radius,  0x00, 0x00, 0x00, 0xFF);
            else if (cell == 1) 
                filledCircleRGBA(_renderer, x, y, radius,  0xFF, 0xFF, 0xFF, 0xFF);
        }
    }

}

void VGameGomoku::click(int x, int y) {
    const Action action { y*_nbI/_height, x*_nbJ/_width };
    auto it = std::find(_legalActions.begin(), _legalActions.end(), action);
    if (it != _legalActions.end()) 
        _selectedAction = std::distance(_legalActions.begin(), it);
    else
        _selectedAction.reset();
}


