
#include "../bot/BotMc.hpp"
#include "../game/GameFind3.hpp"

#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>

TEST_GROUP(GroupBotMc) { };

TEST(GroupBotMc, init1) {
    BotMc b("nSims=100");
    GameFind3 g;
    const int nruns = 10;
    for (int n=0; n<nruns; ++n) {
        auto k = b.chooseAction(g);
        CHECK(*k == 3);
    }
}

TEST(GroupBotMc, init2) {
    BotMc b("");
    GameFind3 g;
    const int nruns = 10;
    for (int n=0; n<nruns; ++n) {
        auto k = b.chooseAction(g);
        CHECK(*k == 3);
    }
}

// TODO

