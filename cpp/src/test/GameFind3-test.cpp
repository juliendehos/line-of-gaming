
#include "../game/GameFind3.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupGameFind3) { };

TEST(GroupGameFind3, init1) {
    GameFind3 g;
    CHECK_EQUAL(0, g.getHistory().size());
    CHECK_EQUAL(0, g.getCurrentPlayer());
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(10, g.getNbLegalActions());
}

TEST(GroupGameFind3, applyAction5) {
    GameFind3 g;
    CHECK_EQUAL(0, g.getCurrentPlayer());
    CHECK_EQUAL(true, g.isPlaying());
    g.applyAction(5);
    CHECK_EQUAL(1, g.getCurrentPlayer());
    CHECK_EQUAL(true, g.isPlaying());

    CHECK_EQUAL(1, g.getHistory().size());
    CHECK_EQUAL(5, g.getHistory()[0]);
}

TEST(GroupGameFind3, applyAction23) {
    GameFind3 g;
    CHECK_EQUAL(0, g.getCurrentPlayer());
    CHECK_EQUAL(true, g.isPlaying());
    g.applyAction(2);
    CHECK_EQUAL(1, g.getCurrentPlayer());
    CHECK_EQUAL(true, g.isPlaying());
    g.applyAction(3);
    CHECK_EQUAL(false, g.isPlaying());
    CHECK_EQUAL(false, g.isWinner(0));
    CHECK_EQUAL(true, g.isWinner(1));

    CHECK_EQUAL(2, g.getHistory().size());
    CHECK_EQUAL(2, g.getHistory()[0]);
    CHECK_EQUAL(3, g.getHistory()[1]);
}

TEST(GroupGameFind3, applyAction9876) {
    GameFind3 g;
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(0, g.getCurrentPlayer());
    g.applyAction(9);
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(1, g.getCurrentPlayer());
    g.applyAction(8);
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(0, g.getCurrentPlayer());
    g.applyAction(7);
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(1, g.getCurrentPlayer());
    g.applyAction(6);
    CHECK_EQUAL(false, g.isPlaying());
    CHECK_EQUAL(true, g.hasTie());

    CHECK_EQUAL(4, g.getHistory().size());
    CHECK_EQUAL(9, g.getHistory()[0]);
    CHECK_EQUAL(8, g.getHistory()[1]);
    CHECK_EQUAL(7, g.getHistory()[2]);
    CHECK_EQUAL(6, g.getHistory()[3]);
}

