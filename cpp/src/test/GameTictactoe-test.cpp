
#include "../game/GameTictactoe.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupGameTictactoe) { };

TEST(GroupGameTictactoe, init1) {
    GameTictactoe g;
    CHECK_EQUAL(9, g.getNbLegalActions());
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(0, g.getCurrentPlayer());
    int k = 0;
    auto legalActions = g.getLegalActions();
    for (int i=0; i<3; ++i)
        for (int j=0; j<3; ++j) {
            CHECK_EQUAL(-1, g.getInBoard(i, j));
            auto ij = legalActions[k++];
            CHECK_EQUAL(i, ij.first);
            CHECK_EQUAL(j, ij.second);
        }
}

TEST(GroupGameTictactoe, applyAction1) {
    GameTictactoe g;
    g.applyAction(1);
    CHECK_EQUAL(8, g.getNbLegalActions());
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(1, g.getCurrentPlayer());

    CHECK_EQUAL(-1, g.getInBoard(0, 0));
    CHECK_EQUAL( 0, g.getInBoard(0, 1));
    CHECK_EQUAL(-1, g.getInBoard(0, 2));
    CHECK_EQUAL(-1, g.getInBoard(1, 0));
    CHECK_EQUAL(-1, g.getInBoard(1, 1));
    CHECK_EQUAL(-1, g.getInBoard(1, 2));
    CHECK_EQUAL(-1, g.getInBoard(2, 0));
    CHECK_EQUAL(-1, g.getInBoard(2, 1));
    CHECK_EQUAL(-1, g.getInBoard(2, 2));

    auto as = g.getLegalActions();
    CHECK_EQUAL(0, as[0].first);    CHECK_EQUAL(0, as[0].second);
    CHECK_EQUAL(0, as[1].first);    CHECK_EQUAL(2, as[1].second);
    CHECK_EQUAL(1, as[2].first);    CHECK_EQUAL(0, as[2].second);
    CHECK_EQUAL(1, as[3].first);    CHECK_EQUAL(1, as[3].second);
    CHECK_EQUAL(1, as[4].first);    CHECK_EQUAL(2, as[4].second);
    CHECK_EQUAL(2, as[5].first);    CHECK_EQUAL(0, as[5].second);
    CHECK_EQUAL(2, as[6].first);    CHECK_EQUAL(1, as[6].second);
    CHECK_EQUAL(2, as[7].first);    CHECK_EQUAL(2, as[7].second);
}

TEST(GroupGameTictactoe, applyAction2) {
    GameTictactoe g;
    g.applyAction(0, 1);
    CHECK_EQUAL(8, g.getNbLegalActions());
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(1, g.getCurrentPlayer());

    CHECK_EQUAL(-1, g.getInBoard(0, 0));
    CHECK_EQUAL( 0, g.getInBoard(0, 1));
    CHECK_EQUAL(-1, g.getInBoard(0, 2));
    CHECK_EQUAL(-1, g.getInBoard(1, 0));
    CHECK_EQUAL(-1, g.getInBoard(1, 1));
    CHECK_EQUAL(-1, g.getInBoard(1, 2));
    CHECK_EQUAL(-1, g.getInBoard(2, 0));
    CHECK_EQUAL(-1, g.getInBoard(2, 1));
    CHECK_EQUAL(-1, g.getInBoard(2, 2));

    auto as = g.getLegalActions();
    CHECK_EQUAL(0, as[0].first);    CHECK_EQUAL(0, as[0].second);
    CHECK_EQUAL(0, as[1].first);    CHECK_EQUAL(2, as[1].second);
    CHECK_EQUAL(1, as[2].first);    CHECK_EQUAL(0, as[2].second);
    CHECK_EQUAL(1, as[3].first);    CHECK_EQUAL(1, as[3].second);
    CHECK_EQUAL(1, as[4].first);    CHECK_EQUAL(2, as[4].second);
    CHECK_EQUAL(2, as[5].first);    CHECK_EQUAL(0, as[5].second);
    CHECK_EQUAL(2, as[6].first);    CHECK_EQUAL(1, as[6].second);
    CHECK_EQUAL(2, as[7].first);    CHECK_EQUAL(2, as[7].second);
}

TEST(GroupGameTictactoe, reset1) {
    GameTictactoe g;
    g.applyAction(0, 1);
    g.reset();
    CHECK_EQUAL(9, g.getNbLegalActions());
    CHECK_EQUAL(true, g.isPlaying());
    CHECK_EQUAL(0, g.getCurrentPlayer());
    int k = 0;
    auto legalActions = g.getLegalActions();
    for (int i=0; i<3; ++i)
        for (int j=0; j<3; ++j) {
            CHECK_EQUAL(-1, g.getInBoard(i, j));
            auto ij = legalActions[k++];
            CHECK_EQUAL(i, ij.first);
            CHECK_EQUAL(j, ij.second);
        }
}

