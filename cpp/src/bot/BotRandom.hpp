#pragma once

#include "../core/Bot.hpp"

#include <random>

class BotRandom : public virtual Bot {
    private:
        std::mt19937 _engine;

    public:
        BotRandom();
        std::optional<int> chooseAction(const Game & game) override;
};

