#include "BotMc.hpp"

#include <cassert>

#include <iostream>

BotMc::BotMc(const std::string & paramsInput) :
    Bot({{"nSims",1000}, {"kWin",1}, {"kTie",0.7}, {"kLoss",-1}})
{
    parse(paramsInput);
}

std::optional<int> BotMc::chooseAction(const Game & game) {
    const int nSims = operator[]("nSims");
    assert(game.isPlaying());
    int nbActions = game.getNbLegalActions();
    int nbSimsPerAction = std::max(1, nSims / nbActions);
    int kBest = 0;
    double sBest = evalAction(game, 0, nbSimsPerAction);
    for (int k=1; k<nbActions; k++) {
        double s = evalAction(game, k, nbSimsPerAction);
        if (s > sBest) {
            kBest = k;
            sBest = s;
        }
        // TODO std::cout << "\n eval(" << k << ") = " << s;
    }
    // TODO std::cout << std::endl;
    return kBest;
}

double BotMc::evalAction(const Game & game, int k0, int nbSims) {
    const double kWin = operator[]("kWin");
    const double kTie = operator[]("kTie");
    const double kLoss = operator[]("kLoss");
    int player0 = game.getCurrentPlayer();
    auto g0 = game.clone();
    g0->applyAction(k0);
    double s = 0.0;
    for (int n=0; n<nbSims; n++) {
        auto gi = g0->clone();
        rollout(*gi);
        if (gi->hasTie())
            s += kTie;
        else if (gi->isWinner(player0))
            s += kWin;
        else s += kLoss;
    }
    return s;
}

void BotMc::rollout(Game & game) {
    while (game.isPlaying()) {
        auto k = _botRandom.chooseAction(game);
        game.applyAction(*k);  // TODO check k ?
    }
}

