#pragma once

#include "BotRandom.hpp"

class BotMc : public virtual Bot {
    private:
        BotRandom _botRandom;

    public:
        BotMc(const std::string & paramsInput);
        std::optional<int> chooseAction(const Game & game) override;

    private:
        double evalAction(const Game & game, int k0, int nsims);
        void rollout(Game & game);
};

