#include "BotRandom.hpp"

#include <cassert>

BotRandom::BotRandom() : _engine(std::random_device{}())
{}

std::optional<int> BotRandom::chooseAction(const Game & game) {
    assert(game.isPlaying());
    int nbActions = game.getNbLegalActions();
    assert(nbActions > 0);
    std::uniform_int_distribution<int> dist(0, nbActions-1);
    int k = dist(_engine);
    assert(k >= 0);
    assert(k < nbActions);
    return k;
}

