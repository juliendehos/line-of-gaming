#include "VGameTictactoe.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>

void VGameTictactoe::display() {

    // name
    std::cout << "tictactoe" << std::endl;

    // board
    std::cout << "   ";
    for (int j=0; j<3; ++j) 
        std::cout << " " << j;
    std::cout << std::endl;
    std::cout << "   ";
    for (int j=0; j<3; ++j) 
        std::cout << "--";
    std::cout << std::endl;
    for (int i=0; i<3; ++i) {
        std::cout << char('a'+i) << " |";
        for (int j=0; j<3; ++j) {
            if (_board[i][j] == 0)
                std::cout << " X";
            else if (_board[i][j] == 1)
                std::cout << " O";
            else
                std::cout << " .";
        }
        std::cout << std::endl;
    }

    // history
    std::cout << "history: ";
    for (const auto & a : _history)
        std::cout << actionToString(a) << " ";
    std::cout << std::endl;

    // actions
    std::cout << "actions: ";
    for (const auto & a : _legalActions)
        std::cout << actionToString(a) << " ";
    std::cout << std::endl;

}

int VGameTictactoe::findLegalAction(const std::string & input) const {

    auto ij = stringToAction(input);
    auto it = std::find(_legalActions.begin(), _legalActions.end(), ij);

    if (it == _legalActions.end())
        throw std::invalid_argument("not a legal action");

    return std::distance(_legalActions.begin(), it);
}

std::string VGameTictactoe::actionToString(const Action & action) {
    std::ostringstream oss;
    oss << char('a' + action.first);
    oss << char('0' + action.second);
    return oss.str();
}

VGameTictactoe::Action VGameTictactoe::stringToAction(const std::string & string) {
    if (string.size() < 2
            or string[0] < 'a' or string[0] > 'c'
            or string[1] < '0' or string[1] > '2')
        throw std::invalid_argument("invalid format (should be: a0 ... c2)");
    return { string[0] - 'a', string[1] - '0' };
}

