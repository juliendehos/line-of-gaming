#include <functional>
#include <map>
#include <memory>

///////////////////////////////////////////////////////////////////////////////
// VGame
///////////////////////////////////////////////////////////////////////////////

#include "VGame.hpp"

using mkVGame_t = std::function<std::unique_ptr<VGame>()>;

extern std::map<std::string, mkVGame_t> VGameFactory;

void checkVGame(const std::string & name);

void showVGameFactory(std::ostream & os);

///////////////////////////////////////////////////////////////////////////////
// bot
///////////////////////////////////////////////////////////////////////////////

#include "../core/Bot.hpp"

using mkBot_t = std::function<std::unique_ptr<Bot>(const std::string &)>;

extern std::map<std::string, mkBot_t> BotFactory;

void checkBot(const std::string & name);

void showBotFactory(std::ostream & os);

