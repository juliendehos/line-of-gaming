#include "VGameFind3.hpp"

#include <iostream>

void VGameFind3::display() {

    // name
    std::cout << "find3" << std::endl;

    // history
    std::cout << "history: ";
    for (int n : getHistory())
        std::cout << n << " ";
    std::cout << std::endl;

}

int VGameFind3::findLegalAction(const std::string & input) const {
    return std::stoi(input);
}

