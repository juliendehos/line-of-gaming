# line-of-gaming (C++)


## Usage

```
nix-shell
cmake -S . -B build
cmake --build build

./build/main-test
./build/main-gui tictactoe human "" mc "nSims=1000"
./build/main-cli ...
./build/main-cmp ...
```


## Game

- GameFind3
- GameTictactoe


## Bot

- BotRandom
- BotMc


## TODO

- BotMcts
- BotMctsPar
- BotAlphaZero

- GameBreakthrough
- GameReversi
- GameGomoku
- GameConnect4
- GameHex
- GameHavannah

- params -> composition ?
- game params ?
- params -> (string-string) ?
- bot stats ?
- game history ?
- single-player, multi-player ?
- pie rule
- player / color

